/* global base_url */

var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}

var offerList = Array();

$(document).ready(function () {
    $(".datepicker-head").datepicker({
        dateFormat: 'mm/dd/yy'
    });

    sdate = localStorage.getItem("start_date");
    edate = localStorage.getItem("end_date");
    ///If local storage have startdate or end date then it will show old date
    if (sdate == null || edate == null) {
        var startDate = moment().startOf('month').format("MM/DD/YYYY");
        var endDate = moment().format("MM/DD/YYYY");
        localStorage.setItem("start_date", startDate);
        localStorage.setItem("end_date", endDate);
        $(".startDate").val(startDate);
        $(".endDate").val(endDate);
    } else {
        var startDate = sdate;
        var endDate = edate;
        $(".startDate").val(sdate);
        $(".endDate").val(edate);
    }
    /*there is no record in current offer thats why updating records*/
    loadData(startDate, endDate);
});

/* For ordering according to Revenue/Date */
$('input[name=selector]').on('change', function () {
    var filter = $('input[name=selector]:checked').val();
    if (filter === 'Revenue') {
        //sort by revenue asceding
        loadOfferData(sortResults(offerList, 'revenue', false));
    } else {
        //sort product date
        loadOfferData(sortResultsByDate(offerList, 'updatedAt', false));
    }
});

$(".datepicker-head").on("change", function dateChange() {

    var sDate = $(".startDate").val();
    var eDate = $(".endDate").val();
    if (moment(sDate) > moment(eDate)) {
        $(".startDate").val(localStorage.getItem("start_date"));
        $(".endDate").val(localStorage.getItem("end_date"));
        //alert("start date should be small then end date");
        swal({
            title: "",
            text: dateErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () {
                // $("#searchText").focus();
            });
    } else {
        localStorage.setItem("start_date", sDate);
        localStorage.setItem("end_date", eDate);
        loadData(sDate, eDate);
    }

});

function loadData(startDate, endDate) {
    localStorage.removeItem("offers");
    offers = '';
    $.ajax({
        //url: base_url + 'offers/getOffers?establishmentId=' + localStorage.getItem('establishmentId') + '&startDate=' + moment(startDate, 'MM/DD/YYYY').format("MM/DD/YYYY") + '&endDate=' + moment(endDate, 'MM/DD/YYYY').format("MM/DD/YYYY") + '&active=true',
        url: base_url + 'offers/getOffers?establishmentId=' + localStorage.getItem('establishmentId') + '&startDate=' + moment('01/01/2018', 'MM/DD/YYYY').format("MM/DD/YYYY") + '&endDate=' + moment().format("MM/DD/YYYY") + '&active=true',
        dataType: "json",
        success: function (result) {
            offerList = result;
            loadOfferData(sortResults(offerList, 'revenue', false));
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("userId");
                        window.location = 'login';
                    });
            }
        }
    });
}


function loadOfferData(result) {
    $("#offer_list").html('');
    if (result.length !== 0) {
        localStorage.setItem("offers", JSON.stringify(result));
        $.each(result, function (i, field) {
            var image = '';
            if (field.offer.product.productPhoto) {
                path = field.offer.product.productPhoto.file.path;
                container = path.split('/')[1];
                filename = path.split('/')[2];
                image = base_url + 'storages/' + container + '/download/' + filename; //'?access_token=' + access_token
            } else {
                image = 'images/placeholder-small.png';
            }

            // To Make Original Price Non Editable 
            // if (field.revenue > 0) {
            //     var href = ''; //style="color:#F9811E"
            //     var classes = '';
            //     var hrefd = '';
            //     var hiddenClass = 'hidden';
            //     var marginStyle = 'style="margin-left:68px"';
            //     var href = 'href="edit_offer?id=' + field.offer.id + '"';
            //     var classes = 'class = "edit"';
            //     var hrefd = 'onclick="deleteOffer(\'' + field.offer.id + '\')"';
            //     var hiddenClass = '';
            //     var marginStyle = '';
            // } else {
            var href = 'href="edit-offer?id=' + field.offer.id + '"';
            var classes = 'class = "edit"';
            var hrefd = 'onclick="deleteOffer(\'' + field.offer.id + '\')"';
            var hiddenClass = '';
            var marginStyle = '';
                //var classesd = 'class = "delete"';
            // }
            var offerlist = '<div class="card">' +
                '<div ' + classes + '>' +
                '<a class="' + hiddenClass + '"' + href + '>Edit</a></div>' +
                '<div style="margin-left:10px;cursor: pointer;">' +
                '<a ' + hiddenClass + ' ' + hrefd + '>Delete</a>' +
                '</div>' +
                '<div ' + marginStyle + ' class="product-img"><img src="' + image + '" alt="No image"></div>' +
                '<div class="product-info">' +
                '<div class="productName"><p><span>Product</span> <a href="current-offer?id=' + field.offer.id + '">' + field.offer.product.name + '</a></p></div>' +
                '<div class="productRevenue"><p data="' + field.revenue + '"><span>Revenue</span> $' + field.revenue + '</p></div>' +
                '<div><p><span>UPC Code</span> ' + field.offer.product.upc + ' </p></div>' +
                '<div class="productDate" style="display:none;"><p data="' + field.offer.updatedAt + '"><span>Date</span> ' + field.offer.updatedAt + '</p></div>' +
                '</div><input type="hidden" id="productRevenue" value="' + field.revenue + '"><input type="hidden" id="productDate" value="' + field.offer.updatedAt + '">' +
                '</div>';
            $("#offer_list").append(offerlist);
        });

    } else {
        var offerlist = '<div class="card">No Offers Found !</div>';
        $("#offer_list").append(offerlist);
    }
}

function deleteOffer(offerId) {
    swal({
        title: "",
        text: deleteConfirmMessage,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ok",
        closeOnConfirm: true
    },
        function () {
            $.ajax({
                url: base_url + 'offers/' + offerId,
                type: "PATCH",
                dataType: "json",
                data: {
                    "active": false ///discussed with Utpal to keep it false for now (21-Mar-2018)
                },
                success: function (response) {
                    swal({
                        title: "",
                        text: "Offer Deleted Successfully.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            // location.reload();
                            loadData();
                        });
                }
            });
        });

}

function sortResults(people, prop, asc) {
    people = people.sort(function (a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
    return people;
    console.log(people);
}

function sortResultsByDate(people, prop, asc) {

    people = people.sort(function (a, b) {
        var a = a.offer.updatedAt;
        var b = b.offer.updatedAt;

        if (a > b) {            // a comes first
            return -1
        } else if (b > a) {     // b comes first
            return 1
        } else {                // equal, so order is irrelevant
            return 0            // note: sort is not necessarily stable in JS
        }
    });
    console.log(people);
    return people;

}