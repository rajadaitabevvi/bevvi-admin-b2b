/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}
productId = '';
offerId = params;
var croppedImageDataURL;

//Accept number only in input
jQuery('.number').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});
var originalQuantity = 0;
var originalEndDate = '';
var showWarning = true;
window.onbeforeunload = function () {
    if (showWarning) {
        return "Do you really want to leave our brilliant application?";
    } else {
        return;
    }
};
$(function () {
    //set Datepicker format
    $("#datepicker").datepicker({
        dateFormat: 'MM dd, yy' //,
        //minDate: 0
    });
    $("#datepicker1").datepicker({
        dateFormat: 'MM dd, yy' //,
        //minDate: 0
    });
    $(".offer2").css('display', 'none'); //On page load hide error

    ///Load Country Drop down
    $.ajax({
        url: 'js/country.json',
        dataType: "application/json",
        cache: false,
        complete: function (abc) {
            var list = '';
            $.each(JSON.parse(abc.responseText), function (name, code) {
                list = '<option value="' + code.name + '">' + code.name + '</option>';
                $(".country_list").append(list);
            });
        }
    });

    offerData = $.grep(JSON.parse(localStorage.getItem("offers")), function (i, e) {
        return i.offer.id == offerId;
    });

    $.ajax({
        url: base_url + 'offers/' + offerId + '?filter={"include":{"product":["productPhoto"]}}',
        success: function (data) {
            originalQuantity = data.totalQty;
            originalEndDate = data.endsAt;
            var productName = $("#productName").val(data.product.name);
            var sku = $("#skuNumber").val(data.product.sku);
            var upc = $("#upcCode").val(data.product.upc);
            var category = $("#category").val(data.product.category);
            categorySelected(data.product.category);
            $("#subCategory option[value='" + data.product.subCategory + "']").attr("selected", true);
            var oPrice = $("#originalPrice").val(data.originalPrice.toFixed(2));
            var dPrice = $("#discountPrice").val(data.salePrice.toFixed(2));
            var quantity = $("#quantity").val(data.totalQty);
            var start_date = $("#datepicker").val(moment(data.startsAt).format("MMMM DD, YYYY"));
            var end_date = $("#datepicker1").val(moment(data.endsAt).format("MMMM DD, YYYY"));
            var itemDescription = $("#itemDescription").val(data.product.description); //product desc
            var size = $("#size").val(data.product.size);
            var unit = $("#unit").val(data.product.units);
            var region = $("#region").val(data.product.region);
            var appellation = $("#appellation").val(data.product.appellation);
            var alc_content = $("#alc_content").val(data.product.alcContent);
            var country = $(".country_list").val(data.product.country);
            var year = $('#year').val(data.product.year); //$("#year").val();
            var varietal = $("#varietal").val(data.product.varietal);
            var description = $("#description").val(data.description);

            var color = $("#color").val(data.product.color);
            var body = $("#body").val(data.product.body);
            var aroma = $("#aroma").val(data.product.aromaAndFlavor);
            var foodPairings = $("#foodPairings").val(data.product.pairings);

            $("#isVolumeDiscountEligible").attr("checked", data.product.isVolumeDiscountEligible);
            $("#isSponsored").attr("checked", data.isSponsored);
            $("#isTaxable").attr("checked", data.product.isTaxable);
            productId = data.product.id;
            if (data.product.productPhoto) {
                path = data.product.productPhoto.file.path;
                container = path.split('/')[1];
                filename = path.split('/')[2];
                image = base_url + 'storages/' + container + '/download/' + filename;
                $("#productImage").attr("src", image);
            } else {
                $("#productImage").attr("src", "images/no-img.png");
            }
        }
    });
});


$("#editBtn a").click(function () {
    $(".upload-img-btn3").show();
});

$('#cropperModel').on('hidden.bs.modal', function () {
    document.getElementById('fileInput').value = '';
    canvas.cropper('destroy');
})

$("#cropperModel").on('shown.bs.modal', function () {
    let img = new Image();
    img.onload = function () {
        context.canvas.height = img.height;
        context.canvas.width = img.width;
        context.drawImage(img, 0, 0);
        canvas.cropper('destroy');
        var cropper = canvas.cropper({
            aspectRatio: 9 / 16
        });
    };
    img.src = cropperImage
});
$('#btnSaveEdit1').click(function () {
    croppedImageDataURL = canvas.cropper('getCroppedCanvas').toDataURL("image/png");
    document.getElementById("productImage").src = croppedImageDataURL;
    croppedImageDataURL = croppedImageDataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
    var blob = b64toBlob(croppedImageDataURL, 'image/png');
    var formData = new FormData();
    formData.append('image', blob, "image123" + Math.random() + '.png');
    formData.append("productId", productId);
    $.ajax({
        url: img_url,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (status) {
            console.log(status);
        },
        error: function (error) {
            console.log(error);
        }
    });
    setTimeout(function () {
        canvas.cropper('reset');
    }, 200);
});
var canvas = $("#canvas"),
    context = canvas.get(0).getContext("2d"),
    $result = $('#result');
$('#fileInput').on('change', function () {
    var reader = new FileReader();
    reader.onload = function (e) {
        var fileInput = document.getElementById('fileInput');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('Selected file is not supported. Please select diffrent image');
            fileInput.value = '';
            return false;
        } else {
            $('#cropperModel').modal('show');
            cropperImage = e.target.result;
        }
    };
    reader.readAsDataURL(this.files[0]);
});

function categorySelected(category) {
    switch (category) {
        case 'Wine':
            $(".subCategory").show();
            $("#subCategory").show().find("option").hide().attr('selected', false);
            $("#subCategory option[data-category='Wine']").show().eq(0).attr('selected', true);
            $("#div_alc_content").show();
            $("#div_region").show();
            $("#div_country").show();
            $("#div_year").show();
            $("#div_varietal").show();
            $("#div_appellation").show();
            $("#div_description").hide();
            $("#div_product_notes").show();
            break;

        case 'Liquor':
            $(".subCategory").show();
            $("#subCategory").show().find("option").hide().attr('selected', false);
            $("#subCategory option[data-category='Liquor']").show().eq(0).attr("selected", true)
            $("#div_alc_content").show();
            $("#div_region").hide();
            $("#div_country").show();
            $("#div_year").hide();
            $("#div_varietal").hide();
            $("#div_appellation").hide();
            $("#div_description").hide();
            $("#div_product_notes").hide();
            break;

        case 'Beer':
            $(".subCategory").hide();
            $("#subCategory").hide().find("option").hide();
            $("#div_alc_content").show();
            $("#div_region").hide();
            $("#div_country").show();
            $("#div_year").hide();
            $("#div_varietal").hide();
            $("#div_appellation").hide();
            $("#div_description").hide();
            $("#div_product_notes").hide();
            break;

        case 'Mixer':
            $(".subCategory").hide();
            $("#subCategory").hide().find("option").hide();
            $("#div_alc_content").hide();
            $("#div_region").hide();
            $("#div_country").hide();
            $("#div_year").hide();
            $("#div_varietal").hide();
            $("#div_appellation").hide();
            $("#div_product_notes").hide();
            break;
    }
}

$("#category").change(function () {
    var category = $('#category').find(":selected").val();
    categorySelected(category);
});

$("#NextBtn").click(function () {
    var validation = FormValidate('next');
    if (validation === true) {
        $("a[href='#product']").trigger("click");
    } else {
        $(".save-edit p.next").html('* Please fill in the above information to move forward. *');
        $("#NextBtn").attr('disable', true);
    }
});

$(".backBtn").click(function () {
    $("a[href='#offer']").trigger("click");
});


///Insert data into Database
$(".createBtn").click(function (data) {
    var validation = FormValidate('create');

    var productName = $("#productName").val();
    var sku = $("#skuNumber").val();
    var upc = $("#upcCode").val();
    var category = $("#category").find(":selected").val();
    var subCategory = $("#subCategory").find(":selected").val();
    var oPrice = $("#originalPrice").val();
    var dPrice = $("#discountPrice").val();
    var quantity = $("#quantity").val();
    var start_date = $("#datepicker").val();
    var end_date = $("#datepicker1").val();
    var itemDescription = $("#itemDescription").val();
    var isVolumeDiscountEligible = $("#isVolumeDiscountEligible").is(":checked");
    var isSponsored = $("#isSponsored").is(":checked");

    var size = $("#size").val();
    var unit = $("#unit").val();
    var alc_content = $("#alc_content").val();
    var region = $("#region").val();
    var appellation = $("#appellation").val();
    var country = $(".country_list").val();
    var year = $('#year').val();
    var subCategory = $('#subCategory').find(":selected").val();
    var varietal = $("#varietal").val();
    var description = $("#description").val();
    var color = $("#color").val();
    var body = $("#body").val();
    var aroma = $("#aroma").val();
    var foodPairings = $("#foodPairings").val();

    if (validation) {
        if (category == 'Wine') {
            var productData = {
                "name": productName,
                "category": category,
                "subCategory": subCategory,
                "size": size,
                "units": unit,
                "description": itemDescription,
                "alcContent": alc_content,
                "region": region,
                "appellation": appellation,
                "country": country,
                "year": year,
                "varietal": varietal,
                "color": color,
                "body": body,
                "aromaAndFlavor": aroma,
                "pairings": foodPairings,
                "subCategory": subCategory
            };
        } else if (category == 'Liquor') {
            var productData = {
                "name": productName,
                "category": category,
                "subCategory": subCategory,
                "size": size,
                "units": unit,
                "description": itemDescription,
                "alcContent": alc_content,
                // "region": region,
                "country": country,
                "subCategory": subCategory
                // "varietal": varietal
            };
        } else if (category == 'Beer') {
            var productData = {
                "name": productName,
                "category": category,
                "size": size,
                "units": unit,
                "description": itemDescription,
                "alcContent": alc_content,
                "country": country
                // "varietal": varietal
            };
        } else if (category == 'Mixer') {
            var productData = {
                "name": productName,
                "category": category,
                "size": size,
                "units": unit,
                "description": itemDescription,
                "upc": upc,
                "sku": sku
            };
        }

        swal({
            title: "",
            text: "Are you sure want to update the offer?",
            //type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Save",
            closeOnConfirm: true
        },
            function () {
                productData.establishmentId = localStorage.getItem('establishmentId');
                productData.isVolumeDiscountEligible = isVolumeDiscountEligible;
                $.ajax({ //Update product details
                    type: 'post',
                    url: base_url + 'products/upsertWithWhere?where={ "establishmentId" : "' + productData.establishmentId + '", "upc" : "' + upc + '"}',
                    data: JSON.stringify(productData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        $.ajax({ ///Update Offer details
                            type: 'patch',
                            url: base_url + 'offers/' + offerId,
                            data: JSON.stringify({
                                "establishmentId": localStorage.getItem('establishmentId'),
                                "description": description, ///wine type dont have description
                                "endsAt": moment(end_date + " 23:59:59", "MMM DD, YYYY HH:mm:ss").utc().format('YYYY-MM-DD HH:mm:ss'),
                                "totalQty": quantity,
                                "active": true,
                                "productId": data.id,
                                isSponsored: isSponsored
                            }),
                            dataType: 'json',
                            contentType: 'application/json',
                            success: function (response) {
                                croppedImageDataURL = 'undefined';//croppedImageDataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
                                // var blob = b64toBlob(croppedImageDataURL, 'image/png');
                                if (croppedImageDataURL != 'undefined') {
                                    var formData = new FormData();
                                    formData.append('image', blob, "image123" + Math.random() + '.png');
                                    formData.append("productId", data.id);
                                    //Image upload to server
                                    $.ajax({
                                        url: img_url,
                                        type: 'POST',
                                        data: formData,
                                        async: false,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success: function (status) {
                                            showWarning = false;
                                            window.location = 'offers';
                                        },
                                        error: function () {
                                            swal({
                                                title: error.status,
                                                text: error.message,
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonClass: "btn-danger",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            },
                                                function () { });
                                        }
                                    })
                                } else {
                                    showWarning = false;
                                    window.location = 'offers';
                                }
                            },
                            error: function (error) {
                                //swal("", "There is some technical issue !", "error");
                                if (error.responseJSON.error.status == 401) {
                                    swal({
                                        title: "",
                                        text: error.responseJSON.error.message,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-danger",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            // localStorage.removeItem("userId");
                                            // window.location = 'login';
                                        });
                                }
                            }
                        });
                    },
                    error: function (response) {
                        if (error.responseJSON.error.status == 401) {
                            swal({
                                title: "",
                                text: error.responseJSON.error.message,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: false
                            },
                                function () {
                                    localStorage.removeItem("userId");
                                    window.location = 'login';
                                });
                        }
                    }
                });
            });
    } else {
        $(".save-edit p.save").html('* Please fill in the above information to move forward. *');
    }
});


function FormValidate(validate) {

    var valid = true;
    var productName = $("#productName").val();
    var sku = $("#skuNumber").val();
    var upc = $("#upcCode").val();
    var category = $("#category").find(":selected").val();
    var oPrice = $("#originalPrice").val();
    var dPrice = $("#discountPrice").val();
    var quantity = $("#quantity").val();
    var start_date = $("#datepicker").val();
    var end_date = $("#datepicker1").val();
    var itemDescription = $("#itemDescription").val();

    var size = $("#size").val();
    var unit = $("#unit").val(); //Added by Us
    var alc_content = $("#alc_content").val();
    var region = $("#region").val();
    var appellation = $("#appellation").val();
    var country = $(".country_list").val();
    var year = $('#year').val();
    var subCategory = $('#subCategory').find(":selected").val(); //$("#year").val();
    var varietal = $("#varietal").val();
    var description = $("#description").val();
    var color = $("#color").val();
    var body = $("#body").val();
    var aroma = $("#aroma").val();
    var foodPairings = $("#foodPairings").val();

    if (validate == 'next') {
        if (productName === '') {
            valid = false;
        }
        // if (sku === '') {
        //     valid = false;
        // }
        // if (upc === '') {
        //     valid = false;
        // }
        if (category === '') {
            valid = false;
        }
        // if (oPrice === '') {
        //     valid = false;
        // }
        // if (dPrice === '') {
        //     valid = false;
        // }
        if (quantity === '') {
            valid = false;
        }
        if (quantity < originalQuantity) {
            swal("", "Quantity can only be increased", "error");
            return false;
        }
        // if (start_date === '') {
        //     valid = false;
        // }
        if (end_date === '') {
            valid = false;
        }
        if (end_date != '') {
            endDateCheck = moment(originalEndDate).format("YYYY-MM-DD");
            if (moment(originalEndDate) > moment(end_date + " 23:59:59", "MMMM DD, YYYY HH:mm:ss")) {
                swal("", "End Date can only be set further than the existing End Date.", "error");
                valid = false;
            } else if (moment(start_date, "MMMM DD, YYYY") > moment(end_date, "MMMM DD, YYYY")) {
                swal("", dateErrorMessage, "error");
                valid = false;
            }
        }

        if (itemDescription === '') {
            valid = false;
        }

        //Validate form
        if (valid === false) {
            return false;
        } else {
            return true;
        }
    } else {
        if (category == 'Wine') {

            // if (region === '') {
            //     valid = false;
            // }
            // if (appellation === '') {
            //     valid = false;
            // }
            // if (country === '') {
            //     valid = false;
            // }
            // if (year === '') {
            //     valid = false;
            // }
            if (color == '') {
                valid = false;
            }
            // if (body == '') {
            //     valid = false;
            // }
            // if (aroma == '') {
            //     valid = false;
            // }
            // if (foodPairings == '') {
            //     valid = false;
            // }
        } else if (category == 'Liquor') {
            // if (region === '') {
            //     valid = false;
            // }
            // if (country === '') {
            //     valid = false;
            // }
            // if (description === '') {
            //     valid = false;
            // }

        } else if (category === 'Beer') {
            // if (description === '') {
            //     valid = false;
            // }

        }
        //Below fields are in all category
        if (size === '') {
            valid = false;
        }
        if (unit === '') {
            valid = false;
        }
        // if (country == '') {
        //     valid = false;
        // }

        //Validate form
        if (valid === false) {
            return false;
        } else {
            return true;
        }
    }
}