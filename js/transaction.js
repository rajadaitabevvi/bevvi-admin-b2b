/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}
var dataset = Array();

$(document).ready(function () {
    sdate = localStorage.getItem("start_date");
    edate = localStorage.getItem("end_date");
    var startDate = sdate? sdate : moment().startOf('month').format("MM/DD/YYYY");
    var endDate = edate ? edate : moment().format("MM/DD/YYYY");
    localStorage.setItem("start_date", startDate);
    localStorage.setItem("end_date", endDate);
    $(".startDate").val(startDate);
    $(".endDate").val(endDate);

    // if (sdate == null || edate == null) {
    //     var startDate = moment().startOf('month').format("MM/DD/YYYY");
    //     var endDate = moment().format("MM/DD/YYYY");
    //     localStorage.setItem("start_date", startDate);
    //     localStorage.setItem("end_date", endDate);
    //     $(".startDate").val(startDate);
    //     $(".endDate").val(endDate);
    // } else {
    //     var startDate = sdate;
    //     var endDate = edate;
    //     $(".startDate").val(sdate);
    //     $(".endDate").val(edate);
    // }

    //set datepicker
    // $(".datepicker-head").datepicker({
    //     dateFormat: 'mm/dd/yy',
    //     changeMonth: true,
    //     changeYear: true
    // });

    var dateFormat = "mm/dd/yy",
      from = $( "#startDate" )
        .datepicker({
        //   defaultDate: localStorage.getItem("start_date") ? localStorage.getItem("start_date") : "+1w",
            // dateFormat: 'MM/DD/YYYY',
            defaultDate: startDate,
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#endDate" ).datepicker({
        // defaultDate: localStorage.getItem("end_date") ? localStorage.getItem("end_date") :"+1w",
        // dateFormat: 'MM/DD/YYYY',
        defaultDate: endDate,
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      console.log(element);
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
      return date;
    } 

    // sdate = localStorage.getItem("TS_date", startDate);
    // edate = localStorage.getItem("TE_date", endDate);

    // ///If local storage have startdate or end date then it will show old date
    // if (sdate == null || edate == null) {
    //     var startDate = moment().startOf('month').format("MM/DD/YYYY");
    //     var endDate = moment().format("MM/DD/YYYY");
    //     $(".startDate").val(startDate);
    //     $(".endDate").val(endDate);
    // } else {
    //     var startDate = sdate;
    //     var endDate = edate;
    //     $(".startDate").val(sdate);
    //     $(".endDate").val(edate);
    // }

    loadData(startDate, endDate);
});

$("#searchBtn").click(function () {
    search = $(".transaction-search").val().trim();
    if (search == '') {
        //alert('Please Enter Order Number');
        swal("", "Please Enter Order Number!", "error");
        $(".confirm-code input").focus();
        //loadTransactions(dataset);
    } else {
        result = $.grep(dataset, function (e) {
            return e.orderNum == search
        });
        if (result.length > 0) {
            loadTransactions(result);
        } else {
            swal("", "No Data found !", "error");
            $(".transaction-search").focus();
        }
    }
});

$(".datepicker-head").on('change', function () {
    startDate = $("#startDate").val();
    endDate = $("#endDate").val();

    localStorage.setItem("start_date", startDate);
    localStorage.setItem("end_date", endDate);

    if (moment(startDate) > moment(endDate)) {
        swal("", dateErrorMessage, "error");
        //alert("Start Date should be small then End Date !");
    }
    if (startDate === '' && endDate === '') {
        swal("", dateErrorMessage, "error");
        //alert("Start Date & End Date should be selected !");
    } else {
        loadData(startDate, endDate);
    }
});

//on order status change
$('#status_selector').on('change', function () {
    var filter = $('#status_selector').val();
    var sort = $('#selector').val();

    if (filter == '') {
        //load all data
        // loadTransactions(dataset);
        startDate = $("#startDate").val();
        endDate = $("#endDate").val();

        loadData(startDate, endDate);
    } else if (filter != null) {
        //load filtered data
        var matches = $.grep(dataset, function (e) {
            return e.orderStatus == filter;
        });
        if (sort == 'Revenue') {
            matches = sortOrders(matches, 'totRevenue', false);
        } else {
            matches = sortOrders(matches, 'paidDate', false);
        }
        loadTransactions(matches);
    } else {
        startDate = $("#startDate").val();
        endDate = $("#endDate").val();
        loadData(startDate, endDate);
        // loadTransactions(dataset);
    }
});


/* For ordering according to Revenue/Date */
$('#selector').on('change', function () {
    var filter = $('#selector').val();
    var status = $('#status_selector').val();
    if (status != '') {
        var result = $.grep(dataset, function (e) {
            return e.orderStatus == status
        });
    } else {
        var result = dataset;
    }

    if (filter === 'Revenue') {
        //sort by revenue asceding
        result = sortOrders(result, 'totRevenue', false);
        loadTransactions(result);
    } else {
        //sort product date
        result = sortOrders(result, 'paidDate', false);
        loadTransactions(result);
    }
});

function loadData(startDate, endDate) {
    startDate = moment(startDate, "MM/DD/YYYY").format('MM/DD/YYYY');
    endDate = moment(endDate, "MM/DD/YYYY").format('MM/DD/YYYY');

    url = base_url + 'orders/transactions?accountId=' + localStorage.getItem("userId") + '&establishmentId=' + localStorage.getItem("establishmentId") + '&startDate=' + startDate + '&endDate=' + endDate + '&access_token=' + access_token;

    $.ajax({
        url: url,
        success: function (response) {
            $("#status_selector").val('');
            $("#selector").val('Date');
            dataset = response;
            dataset = sortOrders(dataset, 'paidDate', false);
            loadTransactions(dataset);
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("userId");
                        window.location = 'login';
                    });
            }
        }
    });
}

function loadTransactions(response) {
    console.log(response);
    $(".transaction-orders").html(response.length + ' Orders Found');

    var total_revenue = 0;
    var total_saving = 0;
    var total_salesTax = 0;
    var total_BevviCommi = 0;
    var total_BevviDisc = 0;
    var total_StoreDisc = 0;
    // var total_Rebate = 0;

    // Clear table before set
    $(".transaction-table tbody").html('');
    if (response.length > 0) {
        $("#selector").removeAttr("disabled");
        $("#status_selector").removeAttr("disabled");
        $.each(response, function (i, data) {
            if (data.orderStatus != 6) {
            
                var status = '';
                revenue = data.totRevenue;
                bevviCommision = data.bevviCommision;
                tax = data.salesTax;
                savings = data.stripeCharges;
                bevviDisc = typeof data.discountApplied.promoDisc === 'number' ? data.discountApplied.promoDisc.toFixed(2) : parseFloat(data.discountApplied.promoDisc).toFixed(2);
                storeDisc = typeof data.discountApplied.volumeDisc === 'number' ? data.discountApplied.volumeDisc.toFixed(2) : parseFloat(data.discountApplied.volumeDisc).toFixed(2);
                /// Pending - 0, Fulfilled - 1, Picked Up - 2
                if (data.orderStatus == 0) {
                    status = 'Pending'
                    // total_revenue = total_revenue + parseFloat(revenue);
                    // total_saving = total_saving + parseFloat(data.savingsOffered);
                    // total_salesTax = total_salesTax + parseFloat(data.salesTax);
                    // total_BevviCommi = total_BevviCommi + parseFloat(data.bevviCommision);
                } else if (data.orderStatus == 1) {
                    total_revenue = total_revenue + parseFloat(revenue);
                    total_saving = total_saving + parseFloat(data.stripeCharges);
                    total_salesTax = total_salesTax + parseFloat(data.salesTax);
                    total_BevviCommi = total_BevviCommi + parseFloat(data.bevviCommision);
                    total_BevviDisc = total_BevviDisc + parseFloat(data.discountApplied.promoDisc);
                    total_StoreDisc = total_StoreDisc + parseFloat(data.discountApplied.volumeDisc);
                    status = 'Fulfilled'
                } else if (data.orderStatus == 2) {
                    total_revenue = total_revenue + parseFloat(revenue);
                    total_saving = total_saving + parseFloat(data.stripeCharges);
                    total_salesTax = total_salesTax + parseFloat(data.salesTax);
                    total_BevviCommi = total_BevviCommi + parseFloat(data.bevviCommision);
                    total_BevviDisc = total_BevviDisc + parseFloat(data.discountApplied.promoDisc);
                    total_StoreDisc = total_StoreDisc + parseFloat(data.discountApplied.volumeDisc);
                    status = 'Delivered';
                } else if (data.orderStatus == 3) {
                    status = 'Rejected';
                    revenue = 0;
                    savings = 0;
                    bevviCommision = 0;
                    bevviDisc = "0.00";
                    storeDisc = "0.00";
                    tax = 0;
                } else if (data.orderStatus == 4) {
                    revenue = 0;
                    tax = 0;
                    savings = 0;
                    bevviCommision = 0;
                    bevviDisc = "0.00";
                    storeDisc = "0.00";
                    status = 'Cancelled';
                } else if (data.orderStatus == 5) {
                    total_revenue = total_revenue + parseFloat(revenue);
                    total_saving = total_saving + parseFloat(data.stripeCharges);
                    total_salesTax = total_salesTax + parseFloat(data.salesTax);
                    total_BevviCommi = total_BevviCommi + parseFloat(data.bevviCommision);
                    total_BevviDisc = total_BevviDisc + parseFloat(data.discountApplied.promoDisc);
                    total_StoreDisc = total_StoreDisc + parseFloat(data.discountApplied.volumeDisc);
                    status = 'Rejected/Age Verification';
                }

                // console.log("INDEX", i);
                // console.log("orderstatus", status);
                // console.log("revenue", revenue);
                // console.log("total_revenue", total_revenue);
                // console.log(" ")




                if (data.discountApplied) {
                    discAmount = parseFloat(data.discountApplied.discountAmount);
                }
                else {
                    discAmount = 0;
                }

                var transact_list = '<tr class="orders">' +
                    '<td><a href="transaction-details?id=' + data.orderNum + '">' + data.orderNum + '</a></td>' +
                    '<td class="status">' + status + '</td>' +
                    '<td>' + moment(data.paidDate).format('MMM D') + '</td>' + //moment(data.paidDate).format('MMM D')
                    '<td>$' + revenue.toFixed(2) + '</td>' +
                    '<td>$' + savings.toFixed(2) + '</td>' + //$0.0
                    '<td>$' + tax.toFixed(2) + '</td>' +
                    '<td>$' + bevviCommision.toFixed(2) + '</td>' + //$0.0
                    '<td class="discount">$' + bevviDisc + '</td>' + //$0.0
                    '<td class="discount">$' + storeDisc + '</td>' + //$0.0
                    // '<td>$' + discAmount.toFixed(2) + '</td>' + //$0.0
                    '<input type="hidden"  class="productRevenue" value="' + data.totRevenue + '">' +
                    '<input type="hidden"  class="productDate" value="' + data.paidDate + '">';
                '</tr>';
                $(".transaction-table tbody").append(transact_list);


                //Calculate total 
                // total_revenue = total_revenue + parseFloat(data.totRevenue);
                // total_saving = total_saving + parseFloat(data.savingsOffered);
                // total_salesTax = total_salesTax + parseFloat(data.salesTax);
                // total_BevviCommi = total_BevviCommi + parseFloat(data.bevviCommision);
                // total_Rebate = total_Rebate + parseFloat(discAmount);
            }
        });
    } else {
        $("#selector").attr("disabled", "disabled");
        $("#status_selector").attr("disabled", "disabled");
        $(".transaction-table tbody").append('<tr><td colspan="7" style="text-align:center;"> No Transactions! </td></tr>');
    }


    $(".total_revenue").text('$' + total_revenue.toFixed(2));
    $(".total_saving").text('$' + total_saving.toFixed(2));
    $(".total_salesTax").text('$' + total_salesTax.toFixed(2));
    $(".total_BevviCommi").text('$' + total_BevviCommi.toFixed(2));
    $(".total_BevviDisc").text('$' + total_BevviDisc.toFixed(2));
    $(".total_StoreDisc").text('$' + total_StoreDisc.toFixed(2));
    // $(".total_Rebate").text('$' + total_Rebate.toFixed(2));
}

function sortOrders(order, field, asc) {
    order = order.sort(function (a, b) {
        if (asc) {
            return (a[field] > b[field]) ? 1 : ((a[field] < b[field]) ? -1 : 0);
        } else {
            return (b[field] > a[field]) ? 1 : ((b[field] < a[field]) ? -1 : 0);
        }
    });
    return order;
}

function sortResultsByDate(order, prop, asc) {

    order = order.sort(function (a, b) {
        var a = a.offer.updatedAt;
        var b = b.offer.updatedAt;

        if (a > b) {            // a comes first
            return -1
        } else if (b > a) {     // b comes first
            return 1
        } else {                // equal, so order is irrelevant
            return 0            // note: sort is not necessarily stable in JS
        }
    });
    console.log(order);
    return order;

}
