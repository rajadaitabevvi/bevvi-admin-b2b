/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Authentication
// var userid = localStorage.getItem('userId');
// if (!userid || !localStorage.getItem('establishmentId')) {
//     window.location = 'login';
// }

// setInterval(function () {
//     //location.reload();
// }, 120000); // 1min = 60000 

var corpOrders = Array();
var openCorpOrders = Array();
var readyToCorpPickup = Array();
var pastCorpOrders = Array();
// var editOrderId = '';
// var oldOrderData = {};
// var oldOrderDetail;
// var newOrderData = {};
// var newOrderDetail;

/* Hide Error Message*/
// $(".order-left .error").css('color', 'red');
// $(".order-left .error").hide();
// $('#order1Save').hide();
// $('#order1loading').hide();
// $('#order1rejectloading').hide();
// $('#order1Cancel').hide();
// $('#order1CancelTop').hide();
// $('.order1ShippingTo').hide();
// $('#order1Shipping').hide();
// $('#order1Delivery').hide();
// $('#order1DeliveryFees').hide();
// $('#order1ShippingFees').hide();

// $(document).ready(function () {
//     loadCorpOrders();
    
//     // setInterval(function() {loadCorpOrders(false)}, 20000);
// });

function loadCorpOrders(shouldLoadOrderData=true, orderNumber=null) {
    $.ajax({
        url: base_url + 'corporders?filter={"where":{"establishmentId":"' + localStorage.getItem("establishmentId") + '"},"include":["account","recipientorders","establishment"], "corporder": "createdAt ASC"}',
        success: function (data) {
            orders.push(data);
            corpOrders = data;
            ///filter open order
            openCorpOrders = $.grep(corpOrders, function (i, e) {
                return i.corpOrderStatus == 0
            });
            openCorpOrders = sortOrders(openCorpOrders, 'deliveryTime', true);
            ///filter ready to pickup orders
            readyToCorpPickup = $.grep(corpOrders, function (i, e) {
                return i.corpOrderStatus == 1
            });
            console.log(readyToCorpPickup);
            //Sort order below function gives updated order first
            readyToCorpPickup = sortOrders(readyToCorpPickup, 'updatedAt', false);

            ///filter past orders
            pastCorpOrders = $.grep(corpOrders, function (i, e) {
                return i.corpOrderStatus == 2 || i.corpOrderStatus == 3 || i.corpOrderStatus == 4 || i.corpOrderStatus == 5
            });
            //console.log(pastCorpOrders);
            //Sort order below function gives updated order first
            pastCorpOrders = sortOrders(pastCorpOrders, 'updatedAt', false);

            LoadSideBar();
            //Load first order's detail
            if (shouldLoadOrderData) {
                if (orderNumber) {
                    loadOrderData(orderNumber);
                } else {
                    if (openOrders.length > 0) {
                        loadOrderData(openOrders[0].orderNumber);
                    } else if (readyToPickup.length > 0) {
                        loadOrderData(readyToPickup[0].orderNumber);
                    } else if (pastOrders.length > 0) {
                        loadOrderData(pastOrders[0].orderNumber);
                    } else {
                        $("#order1").html('<div class="card"><div class="row card-head"><h3>No Orders!</h3></div></div>');
                        $(".order-left").children().attr("style", "pointer-events:none;");
                    }
                }
                
            }
        },
        error: function(data) {
            // console.log(data);
        }
    });
}

function LoadSideBarCorp() {
    /* Load open orders */
    // $("#orders").html('');
    if (openCorpOrders.length > 0) {
        $.each(openCorpOrders, function (i, field) {
            days = '';
            style = '';

            // if (i < 5) { ///Load first 5 records
            orderText = ((typeof field.account != "undefined") ? (field.account.firstName ? field.account.firstName : '') + ' ' + (field.account.lastName ? field.account.lastName : '') : "")
            var orderlist = '<tr data="' + field.corpOrderNum + '" style="' + style + '">' +
            '<td>' +
            '<span>' + field.corpOrderNum + '</span>' +
            '<p>' + orderText + '</p' +
            '</td>' +
            '<td style="white-space : nowrap"></td>' + //sanchit ask to use this format //moment().to(moment(field.pickupTime), true)
            '</tr>';
            $("#orders").append(orderlist);
            // } else { return false; }
        });
    } else {
        $("#orders").append('<tr><td colspan="3" style="text-align:center">No Orders!</td></tr>');
    }


    /* load ready to pickup orders */
    // $("#rtp_orders").html('');
    if (readyToCorpPickup.length > 0) {
        $.each(readyToCorpPickup, function (i, field) {
            // if (i < 5) { ///Load first 5 records
            orderText = ((typeof field.account != "undefined") ? (field.account.firstName ? field.account.firstName : '') + ' ' + (field.account.lastName ? field.account.lastName : '') : "")
            var orderlist = '<tr data="' + field.corpOrderNum + '" style="' + style + '">' +
            '<td>' +
            '<span>' + field.corpOrderNum + '</span>' +
            '<p>' + orderText + '</p' +
            '</td>' +
            '<td style="white-space : nowrap"></td>' + //sanchit ask to use this format //moment().to(moment(field.pickupTime), true)
            '</tr>';
            $("#rtp_orders").append(orderlist);
            // } else {
            //     return false;
            // }
        });
    } else {
        $("#rtp_orders").append('<tr><td colspan="3" style="text-align:center">No Orders!</td></tr>');
    }


    /* Load past orders */
    // $('#c_orders').html('');
    if (pastCorpOrders.length > 0) {
        $.each(pastCorpOrders, function (i, field) {
            $("#order4 .card3").css("display", "none");
            if (i == 0) {
                if (field.status === 2) {
                    tick = '<i class="fa fa-check" aria-hidden="true"></i>';
                    title = 'Order <br/>Completed ';
                } else if (field.status === 3) {
                    tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                    title = 'Order <br/>Rejected ';
                } else if (field.status === 4) {
                    tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                    title = 'Order <br/>Cancelled ';
                } else if (field.status === 5) {
                    tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                    title = 'Order <br/>Cancelled (Invalid ID) ';
                }
                $("#order4 tbody .status_tick").html(tick);
                $("#order4 tbody .status_title").html(title);
                $("#order4 tbody .date").html(moment(field.createdAt).format('MMM DD. ddd'));
                $("#order4 tbody .orderNumber").html(field.orderNumber);
                $("#order4 tbody .orderDeliveryTime").html(moment(field.deliveryTime).format('hh:mm a'));
                $("#order4 tbody .orderPlaceTime").html(moment(field.createdAt).format('hh:mm a'));
                $("#order4 tbody .subtotal").html('$' + field.totalAmount.toFixed(2));

            } else {
                if (field.status === 2) {
                    tick = '<i class="fa fa-check" aria-hidden="true"></i>';
                    title = 'Order <br/>Completed ';
                } else if (field.status === 3) {
                    tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                    title = 'Order <br/>Rejected ';
                } else if (field.status === 4) {
                    tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                    title = 'Order <br/>Cancelled ';
                } else if (field.status === 5) {
                    tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                    title = 'Order <br/>Cancelled (Invalid ID) ';
                }

                var orderList;
                if (field.isShipping) {
                    orderList =
                        '<tr data="' + field.orderNumber + '">' +
                        '<input class="orderRevenue" type="hidden" value="' + field.createdAt + '">' +
                        '<input class="orderDate" type="hidden" value="' + field.totalAmount + '">' +
                        '<td>' + tick + '</td>' +
                        '<td>' + title + '</td>' +
                        '<td>' + moment(field.createdAt).format('MMM DD. ddd') + '</td>' +
                        '<td class="orderNumber">' + field.orderNumber + '</td>' +
                        '<td>' + moment(field.deliveryTime).format('hh:mm a') + '</td>' +
                        '<td>' + moment(field.createdAt).format('hh:mm a') + '</td>' +
                        '<td>$' + field.totalAmount.toFixed(2) + '</td>' +
                        '</tr>';
                } else {
                    orderList =
                        '<tr data="' + field.orderNumber + '">' +
                        '<input class="orderRevenue" type="hidden" value="' + field.createdAt + '">' +
                        '<input class="orderDate" type="hidden" value="' + field.totalAmount + '">' +
                        '<td>' + tick + '</td>' +
                        '<td>' + title + '</td>' +
                        '<td>' + moment(field.createdAt).format('MMM DD. ddd') + '</td>' +
                        '<td class="orderNumber">' + field.orderNumber + '</td>' +
                        '<td>' + moment(field.deliveryTime).format('hh:mm a') + '</td>' +
                        '<td>' + moment(field.createdAt).format('hh:mm a') + '</td>' +
                        '<td>$' + field.totalAmount.toFixed(2) + '</td>' +
                        '</tr>';
                }
                
                $("#completed tbody").append(orderList);
            }

            ///Load sidebar
            // if (i < 2) { ///Load first 2 records
            if (field.status == 2) {
                status = 'Done';
            } else if (field.status == 3) {
                status = 'Rejected';
            } else if (field.status == 4) {
                status = 'Cancelled';
            } else if (field.status == 5) {
                status = 'Cancelled (InvalidId)';
            }
            var orderlist = '<tr data="' + field.orderNumber + '">' +
                '<td>' +
                '<span>' + field.orderNumber + '</span>' +
                '<p>' + field.qty + ' items for ' + ((typeof field.account != "undefined") ? (field.account.firstName ? field.account.firstName : '') + ' ' + (field.account.lastName ? field.account.lastName : '') : "") + '</p>' + 
                '</td>' +
                '<td>' + status + '</td>' +
                '</tr>';
            $("#c_orders").append(orderlist);
            // }
        });
    } else {
        $("#c_orders").append('<tr><td colspan="3" style="text-align:center">No Orders!</td></tr>');
        $("#order4 tbody").html('<tr><td colspan="7" style="text-align:center">No Orders!</td></tr>');
    }

}

$("#order4 tr").click(function (e) {
    // console.log($(this).val());
    loadOrderData($(this).text());
});


function loadCorpOrderData(OrderNumber) {
    console.log(OrderNumber);
    singleOrder = $.grep(corpOrders, function (i, e) {
        return i.corpOrderNum == OrderNumber
    });

    console.log(singleOrder);
    //Set it selected
    $('.table tr').removeClass("activeOrder");
    $('[data="' + OrderNumber + '"]').addClass("activeOrder");


    var order = '';
    var status = '';
    oldOrderData = '';
    oldOrderDetail = '';
    newOrderData = '';
    newOrderDetail = '';
    isCorpOrder = true;

    //Assign order data
    order = singleOrder[0];
    status = singleOrder[0].corpOrderStatus;
    $("#order1_itemList").html('');
    $("#order2_itemList").html('');
    $('#confirm').show();
    $('#cancel').show();
    // For Corp Order
    $('.isCorpOrder').show();
    $('.isRetailOrder').hide();

    if (status == 0) {
        console.log('STATUS IS ', status)
        $("#corpOrder1Accept").attr("data-order_id", order.id);
        $("#corpOrder1Accept").attr("data-user", order.account.firstName);

        $("#corpOrder1Reject").attr("data-order_id", order.id);
        $("#corpOrder1Reject").attr("data-user", order.account.firstName);
        /*Set Header */
        $('#order1 h3 .span0').html((order.account.firstName ? order.account.firstName : '') + ' ' + (order.account.lastName ? order.account.lastName : ''));

        $('#order1 .card-head-r .orderNumber').html('Order Number: ' + order.corpOrderNum);
        $('#order1 .card-head-r p a').attr('href', 'print-receipt?id=' + order.corpOrderNum);
        $('#order1 .card-head-r p button').attr('data-order', order.id);
        $('#order1 .card-head-r p button').attr('id', 'btn'+order.id);
        $('#order1 .card-head-r p button').show();
        $('#order1Save').hide();
        $('#order1loading').hide();
        $('#order1rejectloading').hide();
        $('#order1originalTotal').hide();
        $('#order1Cancel').hide();
        $('#order1CancelTop').hide();
        $('#corpOrder1Accept').show();
        $('#order1Reject').show();
        $('.note-order').hide();

        if (order.decision != undefined || order.decision === 'fail' || order.decision === 'pass' || order.decision === 'review') {
            $('#order1 #riskScore').show();
            if (order.decision === 'fail') {
                $('#order1 #riskScore').html('UnSafe Order');
            } else if (order.decision === 'pass') {
                $('#order1 #riskScore').html('Safe Order');
            } else if (order.decision === 'review') {
                $('#order1 #riskScore').html('Order Authentcity is Pending');
            } 
        } else {
            $('#order1 #riskScore').hide();
        }

        /*Set Header */

        var total_amount = 0;
        var tax_amount = 0;
        var paid_amount = 0;

        $("#order1_giftNote").html(order.giftNoteString)
        $("#order1_giftWrap").html(order.giftWrap ? 'Yes' : 'No')

        $.each(order.products, function (j, productDetail) {
            // console.log(productDetail.product.id);
            var product_row = '<tr id="' + productDetail.productId + '">' +
                '<td id="productName"><b>' + productDetail.name +
                '</b><br>' +
                '<span>' + productDetail.size + ' ' + productDetail.units + '</span>' +
                '</b><br>' +
                // '<span>' + productDetail.category + '</span>' +
                '</td>' +
                '<td id="productQty" data-product_id="'+productDetail.productId+'">' + productDetail.quantity +
                '</td>' +
                // '<td id="textboxQty" data-product_id="'+productDetail.product.id+'" style="display:none"><input type="text" name="changeQty" id="changeQty-'+productDetail.product.id+'" value="'+productDetail.quantity+'" data-product_id="' + productDetail.product.id + '">'+
                // '</td>' + 
                '<td id="productPrice">$' + productDetail.price.toFixed(2) + '</td>' +
                '</tr>';
            $("#order1_itemList").append(product_row);

            //Calculate Total Amount
            // total_amount = parseFloat(total_amount) + parseFloat(order.subTotal);
        });

        $(".order1_subtotal").html('$' + order.subTotal.toFixed(2));
        if (order.taxes != null) {
            $(".order1_taxamount").html('$' + order.taxes.toFixed(2));
        } else {
            $(".order1_taxamount").html('$0.00');
        }
        $('#order1DeliveryFees').hide();
        $('#order1ShippingFees').show();
        $(".shippingFee").html('$' + order.shippingCharges.toFixed(2));
        $(".order1_paidamount").html('$' + order.orderTotal.toFixed(2));


        $('.containers').html('')
        // Set Table for Recipients
        $.each(order.recipientorders, function(j, recipient) {
            console.log(j)
            let NAME = `${recipient.firstName} ${recipient.lastName}`;
            let ADDRESS = `${recipient.streetAddress}, ${recipient.state}, ${recipient.zipcode}`;
            let PHONE = `${recipient.phoneNum}`
            let EMAIL = `${recipient.email}`
            var leftPanel = '<p><span class="header-title">Name: </span>' + NAME + '</p>' +
                '<p><span class="header-title">Address: </span>' + ADDRESS + '</p>' +
                '<p><span class="header-title">Phone: </span>' + PHONE + '</p>' +
                '<p><span class="header-title">Email: </span>' + EMAIL + '</p>';

            var topPanel = 
                '<div class="border-1 p-0">' + 
                    '<div class="d-flex">'+
                        '<div class="w-100 leftPanel b-r-0">'+
                        leftPanel + 
                        '</div>'+
                    '</div>'+
                    '<div class="bottomPanel">' +
                        '<h3 class="header-title">Products To Shipped:</h3>'+
                        '<div class="d-flex-wrap panel-'+j+'">'+
                        '</div>'
                    '</div>'
                '<div>';
            $('.containers').append(topPanel)
            
            console.log(recipient)
            // $('.leftPanel').append(leftPanel);
            $.each(recipient.products, function(k, product) {
                var productPanel = `<div><span>${product.name}</span><span>${product.quantity} Qty</span></div>`
                $('.panel-'+j).append(productPanel)
            });
        });

        setTab(status);
    } else if (status == 1) {
        $("#corpOrder2PickedUpBtn").attr("data-order_id", order.id);
        $("#corpOrder2PickedUpBtn").attr("data-user", order.account.firstName);

        $("#corpOrder2RejectBtn").attr("data-order_id", order.id);
        $("#corpOrder2RejectBtn").attr("data-user", order.account.firstName);
        $('#order2 .card-head-r p button').attr('data-order', order.id);
        $('#order2 .card-head-r p button').attr('id', 'btn'+order.id);
        $('#order2 .card-head-r p button').show();

        /*Set Header */
        //3 items are ready for user
        /*if (order.qty > 1) {
            $('#order2 h3 .span0').html(order.qty + ' items are ready for ' + order.account.firstName + ' ' + order.account.lastName);
        } else {
            $('#order2 h3 .span0').html(order.qty + ' item is ready for ' + order.account.firstName + ' ' + order.account.lastName);
        }*/

        $('#order2Save').hide();
        $('#order2loading').hide();
        $('#order2rejectloading').hide();
        $('#order2originalTotal').hide();
        $('#order2Cancel').hide();
        $('#corpOrder2PickedUpBtn').show();
        $('#corpOrder2RejectBtn').show();
        $('#order2ShippingTo').hide();
        $('#order2DeliveryFees').hide();
        $('#order2ShippingFees').hide();

        $('#order2 h3 .span0').html((order.account.firstName ? order.account.firstName : '') + ' ' + (order.account.lastName ? order.account.lastName : ''));
        
        if (!order.isShipping) {
            if (moment().diff(order.deliveryTime, 'days') === 0) {
                $('#order2 h3 .span1').html('Today');
            } else if (moment().diff(order.deliveryTime, 'days') === -1) {
                $('#order2 h3 .span1').html('Tomorrow'); //Dec 1. Fri
            } else if (moment().diff(order.deliveryTime, 'days') < 0 || moment().diff(order.deliveryTime, 'days') > 0) {
                $('#order2 h3 .span1').html(moment(order.deliveryTime).format("MMM D. ddd")); //Dec 1. Fri
            }
            $('#order2 h3 .span1').html(moment(order.deliveryTime).format("MMM DD. ddd hh:mma")); //Dec 1. Fri
            $('#order2 h3 .span2').html(moment(order.deliveryTime).format('h:mm a'));
        }

        $('#order2 .card-head-r .orderNumber').html('Order Number: ' + order.orderNumber);
        $('#order2 .card-head-r p a').attr('href', 'print-receipt?id=' + order.orderNumber);
        /*Set Header */

        if (order.decision != undefined || order.decision === 'fail' || order.decision === 'pass' || order.decision === 'review') {
            $('#order2 #riskScore').show();
            if (order.decision === 'fail') {
                $('#order2 #riskScore').html('UnSafe Order');
            } else if (order.decision === 'pass') {
                $('#order2 #riskScore').html('Safe Order');
            } else if (order.decision === 'review') {
                $('#order2 #riskScore').html('Order Authentcity is Pending');
            } 
        } else {
            $('#order2 #riskScore').hide();
        }

        if (order.deliveryInstructions == "" || order.deliveryInstructions === undefined) {
            $('#order2 h3 .deliveryInstructions').html("<b>Delivery Instructions</b>: ").hide();
        } else {
            $('#order2 h3 .deliveryInstructions').html("<b>Delivery Instructions</b>: " + order.deliveryInstructions).show();
        }

        var total_amount = 0;
        var tax_amount = 0;
        var paid_amount = 0;

        $.each(order.products, function (j, productDetail) {
            // console.log(productDetail.product.id);
            var product_row = '<tr id="' + productDetail.productId + '">' +
                '<td id="productName"><b>' + productDetail.name +
                '</b><br>' +
                '<span>' + productDetail.size + ' ' + productDetail.units + '</span>' +
                '</b><br>' +
                // '<span>' + productDetail.category + '</span>' +
                '</td>' +
                '<td id="productQty" data-product_id="'+productDetail.productId+'">' + productDetail.quantity +
                '</td>' +
                // '<td id="textboxQty" data-product_id="'+productDetail.product.id+'" style="display:none"><input type="text" name="changeQty" id="changeQty-'+productDetail.product.id+'" value="'+productDetail.quantity+'" data-product_id="' + productDetail.product.id + '">'+
                // '</td>' + 
                '<td id="productPrice">$' + productDetail.price.toFixed(2) + '</td>' +
                '</tr>';
            $("#order2_itemList").append(product_row);

            //Calculate Total Amount
            // total_amount = parseFloat(total_amount) + parseFloat(order.subTotal);
        });
        $(".order2_subtotal").html('$' + order.subTotal.toFixed(2));
        if (order.taxes != null) {
            $(".order2_taxamount").html('$' + order.taxes.toFixed(2));
        } else {
            $(".order2_taxamount").html('$0.00');
        }
        $('#order2DeliveryTo').hide();
        $('#order2ShippingFees').show();
        $(".shippingFee").html('$' + order.shippingCharges.toFixed(2));
        
        $(".order2_paidamount").html('$' + order.orderTotal.toFixed(2));

        $('.containersOrder').html('')
        // Set Table for Recipients
        $.each(order.recipientorders, function(j, recipient) {
            console.log(j)
            let NAME = `${recipient.firstName} ${recipient.lastName}`;
            let ADDRESS = `${recipient.streetAddress}, ${recipient.state}, ${recipient.zipcode}`;
            let PHONE = `${recipient.phoneNum}`
            let EMAIL = `${recipient.email}`
            var leftPanel = '<p><span class="header-title">Name: </span>' + NAME + '</p>' +
                '<p><span class="header-title">Address: </span>' + ADDRESS + '</p>' +
                '<p><span class="header-title">Phone: </span>' + PHONE + '</p>' +
                '<p><span class="header-title">Email: </span>' + EMAIL + '</p>';

            var rightPanel = '<p>' +
                '<span class="header-title">Carrier Name:</span>' +
                '<input type="text" name="carrierName-'+ recipient.id+ '" id="carrierName-'+
                recipient.id+ '" class="f-16" placeholder="UPS" value="'+recipient.carrierName+'"' +
                '/>' +
            '</p>' + '<p>' +
                '<span class="header-title">Tracking No:</span>' +
                '<input type="text" name="trackingNo-'+ recipient.id +'" id="trackingNo-'+ recipient.id +'" class="f-16" placeholder="" value="'+recipient.trackingNo+'"/>' +
            '</p>' +
            '<p class="justify-end-m-10-30">' +
                `<button type="button" class="btn btn-ready3" name="${recipient.id}" onclick="saveTracking('${recipient.id}')">Save</button>` +
            '</p>';

            var topPanel = 
                '<div class="border-1 p-0">' + 
                    '<div class="d-flex">'+
                        '<div class="w-50 leftPanel">'+
                            leftPanel + 
                        '</div>'+
                        '<div class="w-50 rightPanel">'+
                            rightPanel + 
                        '</div>'+
                    '</div>'+
                    '<div class="bottomPanel">' +
                        '<h3 class="header-title">Products To Shipped:</h3>'+
                        '<div class="d-flex-wrap panel-'+j+'">'+
                        '</div>'
                    '</div>'
                '<div>';
            $('.containersOrder').append(topPanel)
            
            console.log(recipient)
            // $('.leftPanel').append(leftPanel);
            $.each(recipient.products, function(k, product) {
                var productPanel = `<div><span>${product.name}</span><span>${product.quantity} Qty</span></div>`
                $('.panel-'+j).append(productPanel)
            });
        });

        setTab(status);

    } else if (status == 2 || status == 3 || status == 4 || status == 5) {
        /*Set Header */
        $('#order5 h3 .span0').html(order.account.firstName + ' Picked at ' + moment(order.deliveryTime).format('h:mm a'));
        $("#order5RejectBtn").attr("data-order_id", order.id);
        $("#order5RejectBtn").attr("data-user", order.account.firstName);

        if (!order.isShipping) {
            if (moment().diff(order.deliveryTime, 'days') === 0) {
                $('#order5 h3 .span1').html('Today');
            } else if (moment().diff(order.deliveryTime, 'days') === -1) {
                $('#order5 h3 .span1').html('Tomorrow'); //Dec 1. Fri
            } else if (moment().diff(order.deliveryTime, 'days') < 0 || moment().diff(order.deliveryTime, 'days') > 0) {
                $('#order5 h3 .span1').html(moment(order.deliveryTime).format("MMM D. ddd hh:mma")); //Dec 1. Fri
            }
            $('#order5 h3 .span1').html(moment(order.deliveryTime).format("MMM DD. ddd hh:mma")); //Dec 1. Fri
            $('#order5 h3 .span2').html(moment(order.deliveryTime).format('h:mm a'));
        }
        // $('#order5 h3 .span1').css("display", "none");
        // $('#order5 h3 .span2').css("display", "none");

        $('#order5 .card-head-r .orderNumber').html('Order Number: ' + order.orderNumber);
        $('#order5 .card-head-r p a').attr('href', 'print-receipt?id=' + order.orderNumber);
        $('#order5 .card-head-r p button').attr('data-order', order.id);
        $('#order5 .card-head-r p button').attr('id', 'btn'+order.id);
        $('#order5 .card-head-r p button').show();
        $('#order5Save').hide();
        $('#order5loading').hide();
        $('#order5rejectloading').hide();
        $('#order5originalTotal').hide();
        $('#order5Cancel').hide();
        $('#order5RejectBtn').hide();
        $('#order5ShippingTo').hide();
        $('#order5DeliveryFees').hide();
        $('#order5ShippingFees').hide();

        if (status === 2) {
            $('#order5editText').hide();
            $('#order5RejectBtn').show();
        }

        if (order.decision != undefined || order.decision === 'fail' || order.decision === 'pass' || order.decision === 'review') {
            $('#order5 #riskScore').show();
            if (order.decision === 'fail') {
                $('#order5 #riskScore').html('UnSafe Order');
            } else if (order.decision === 'pass') {
                $('#order5 #riskScore').html('Safe Order');
            } else if (order.decision === 'review') {
                $('#order5 #riskScore').html('Order Authentcity is Pending');
            } 
        } else {
            $('#order5 #riskScore').hide();
        }
        /*Set Header */

        $('#order5 h3 .span0').html((order.account.firstName ? order.account.firstName : '') + ' ' + (order.account.lastName ? order.account.lastName : ''));

        let addressHtml = "";
        if (order.companyName != undefined && order.companyName != '') {
            addressHtml += order.companyName + "<br>";
        }
        if (order.aptNumber != undefined && order.aptNumber != '') {
            addressHtml += "<b>Apt/Suite#</b>: " + order.aptNumber + "<br>";
        }
        if (order.address != undefined) {
            addressHtml += order.address + "<br>";
        }
        if (!order.orderForOther && order.account.phoneNumber != undefined) {
            addressHtml += order.account.phoneNumber;
        } else if (order.orderForOther){
            addressHtml += order.orderPhoneNumber;
        }
        $('#order5 h3 .small').html(addressHtml);

        if (order.deliveryInstructions == "" || order.deliveryInstructions === undefined) {
            $('#order5 h3 .deliveryInstructions').html("<b>Delivery Instructions</b>: ").hide();
        } else {
            $('#order5 h3 .deliveryInstructions').html("<b>Delivery Instructions</b>: " + order.deliveryInstructions).show();
        }

        switch (order.status) {
            case 2: $(".orderStatus").html("Delivered");
                break;
            case 3: $(".orderStatus").html("Rejected");
                break;
            case 4: $(".orderStatus").html("Cancelled");
                break;
            case 5: $(".orderStatus").html("Rejected due to Invalid ID");
                break;
        }

        var total_amount = 0;
        var tax_amount = 0;
        var paid_amount = 0;
        $("#order5_itemList").html('');
        $.each(order.orderdetails, function (j, productDetail) {
            var product_row = '<tr id="' + productDetail.product.id + '">' +
            '<td id="productName"><b>' + productDetail.product.name +
                '</b><br>' +
                '<span>' + productDetail.product.size + ' ' + productDetail.product.units + '</span>' +
                '<br>' +
                '<span>' + productDetail.product.category + '</span>' +
                '</td>' +
                '<td>' + productDetail.quantity + '</td>' +
                '<td id="productPrice">$' + productDetail.price.toFixed(2) + '</td>' +
                '</tr>';
            $("#order5_itemList").append(product_row);

            //Calculate Total Amount
            total_amount = parseFloat(total_amount) + parseFloat(productDetail.price * productDetail.quantity);
        });
        var paidAmount = parseFloat((((total_amount + parseFloat(order.tax) + parseFloat(order.shippingFee)) - order.discountApplied.discountAmount + parseFloat(order.deliveryFee) + parseFloat(order.tipAmt))).toFixed(2))
        $(".order5_subtotal").html('$' + total_amount.toFixed(2));
        if (order.tax != null) {
            $(".order5_taxamount").html('$' + order.tax.toFixed(2));
        } else {
            $(".order5_taxamount").html('$0.00');
        }
        //$(".order5_taxamount").html('$' + order.tax.toFixed(2));
        $(".order5_paidamount").html('$' + order.totalAmount.toFixed(2));

        if (order.isShipping) {
            $('#order5ShippingTo').show();
            $('#order5DeliveryTo').hide();
            $('#order5ShippingFees').show();
            console.log(order.shippingFee)
            $('.shippingFee').html('$' + parseFloat(order.shippingFee).toFixed(2));
        } else {
            $('#order5ShippingTo').hide();
            $('#order5DeliveryTo').show();
            $('#order5ShippingFees').hide();
        }

        if (order.deliveryFee != null) {
            if (order.deliveryFee === 0) {
                $('#order5DeliveryFees').hide();
            } else {
                $('#order5DeliveryFees').show();
            }
            $(".order5_deliveryfees").html('$' + order.deliveryFee.toFixed(2));
        } else {
            $('#order5DeliveryFees').hide();
            $(".order5_deliveryfees").html('$0.00');
        }
        // $(".order5_deliveryfees").html('$' + order.deliveryFee.toFixed(2));

        $(".order5_tipamount").html('$' + order.tipAmt.toFixed(2));

        if (typeof order.discountApplied != "undefined" && typeof order.discountApplied.discountAmount != "undefined") {
            $(".order5_rewards_bevvi").html('(' + parseFloat(order.discountApplied.promoDisc).toFixed(2) + ')');
            $(".order5_rewards_store").html('(' + parseFloat(order.discountApplied.volumeDisc).toFixed(2) + ')');
        } else {
            $(".order5_rewards_bevvi").html('$0.00');
            $(".order5_rewards_store").html('$0.00');
        }

        // $("#order4").hide();
        // $("#order5").show();
        // $("#order5").addClass("in active");
        // $(".order-left li").find("a").parent("li").removeClass("active in");
        // $(".order4").parent("li").addClass("active in");
        setTab(status);
    }

}

///li title click
// $(".order-left li a").click(function (e) {

//     // var d = $(this).find('a').attr("class");
//     var d = $(this).attr("class");

//     $("#" + d).show();
//     if (d == 'order1') { //openorder
//         if (!openOrders.length) {
//             swal("", "No Orders! ", "error");
//             $("#order1").hide();
//         } else {
//             loadOrderData(openOrders[0].orderNumber);
//         }
//     }
//     if (d == 'order2') { //ready to pickup
//         if (!readyToPickup.length) {
//             swal("", "No Orders! ", "error");
//             $("#order2").hide();
//         } else {
//             loadOrderData(readyToPickup[0].orderNumber);
//         }
//     }
//     if (d === 'order4') {
//         $('.table tr').removeClass("activeOrder");
//         $(".order4").parent("li").addClass("active in");
//         $("#order4").show();
//         $("#order4 .card3").css("display", "block");
//         $("#order4").addClass('in ');
//         $('#order1').hide();
//         $('#order2').hide();
//         $('#order3').hide();
//         $('#order5').hide();
//         // $('#order4').hide();
//         // if (!pastOrders.length) {
//         //     swal("", "No Orders! ", "error");
//         //     //alert("No Orders ! ");
//         // } else {
//         //     $('#order5').show();
//         // }
//     }
// });


//order-popup-close(Added on 4-6-18 start)
$('.order-close-btn').click(function () {
    $('.order-right').css({ transform: 'translate(100%)' });
});
//order-popup-close(Added on 4-6-18 end)

function setTab(orderStatus) {

    //Remove active class from li
    $(".order-left li").find("a").parent("li").removeClass("active in");

    ///Hide all divs
    $("#order1").hide();
    $("#order2").hide();
    $("#order3").hide();
    $("#order4").hide();
    $("#order5").hide();

    switch (orderStatus) {
        case 0: //orders
            $("#order1").show();
            $(".order1").parent("li").addClass("active in");
            $("#order1").addClass('in ');
            break;

        case 1: //ready to pickup
            $("#order2").show();
            $(".order2").parent("li").addClass("active in");
            $("#order2").addClass('in ');

            break;

        case 2: //past orders
            $(".order5").parent("li").addClass("active in");
            $("#order5").show();
            $("#order5 .card3").css("display", "block");
            $("#order5").addClass('in ');
            break;

        case 3:
            $(".order5").parent("li").addClass("active in");
            $("#order5").show();
            $("#order5 .card3").css("display", "block");
            $("#order5").addClass('in ');
            // $(".order4").parent("li").addClass("active in");
            // $("#order4").show();
            // $("#order4 .card3").css("display", "block");
            // $("#order4").addClass('in ');
            break;

        case 4:
            // $(".order4").parent("li").addClass("active in");
            // $("#order4").show();
            // $("#order4 .card3").css("display", "block");
            // $("#order4").addClass('in ');
            $(".order5").parent("li").addClass("active in");
            $("#order5").show();
            $("#order5 .card3").css("display", "block");
            $("#order5").addClass('in ');
            break;
        case 5:
            // $(".order4").parent("li").addClass("active in");
            // $("#order4").show();
            // $("#order4 .card3").css("display", "block");
            // $("#order4").addClass('in ');
            $(".order5").parent("li").addClass("active in");
            $("#order5").show();
            $("#order5 .card3").css("display", "block");
            $("#order5").addClass('in ');
            break;
    }
}


//To click all past orders
$(".all-orders a").click(function () {
    $("#order4 .card3").css("display", "block");
});


//Search Button Click event
$(".btn-searchcode").click(function () {
    var search = $(".confirm-code input").val().trim();
    if (search === null || search === '') {
        swal("", "Please Enter Order Number!", "error");
        $(".confirm-code input").focus();
        //alert('Please Enter Order Number!');
    } else {
        singleOrder = $.grep(orders, function (i, e) {
            return i.orderNumber == search
        });

        if (singleOrder.length > 0) {
            loadOrderData(search);
            $(".confirm-code input").val('');
        } else {
            swal({
                title: "",
                text: "Invalid Order Number",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
                function () {
                    location.reload();
                });
        }
    }
});



///Accept Order
$("#corpOrder1Accept").click(function () {

    $(".modal-body p").html("Accept " + $("#corpOrder1Accept").attr("data-user") + "'s Order ?");
    // $('#corpOrder1Accept').hide();
    // $('#order1loading').show();
    $('#confirm').on('click', corpOrder1Accept);
    $('#cancel').show();
    $('#alterDialog').modal('show');
    //status = 1; //Order accepted and pickupPending
});

function corpOrder1Accept() {
    $('#alterDialog').modal('hide');
    $('#corpOrder1Accept').hide();
    $('#order1loading').show();
    var order_id = $("#corpOrder1Accept").attr("data-order_id");
    $.ajax({
        url: corp_order_url,
        data: {
            order_id: order_id,
            action: "accept",
            access_token: access_token
        },
        type: "POST",
        success: function (response) {
            
            if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
                $(".modal-body p").html("Session Expired, Please login agan!");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', logout);
                $('#alterDialog').modal('show');
                $('#corpOrder1Accept').show();
                $('#order1loading').hide();
                // swal({
                //     title: "",
                //     text: errorMessage,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: false
                // },
                //     function () {
                //         localStorage.removeItem("userId");
                //         window.location = 'login';
                //         // location.reload();
                //     });
            } else if (response.status == 'error') { // card unauthenticated
                // swal({
                //     title: "",
                //     text: response.message,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                // function () {
                //     $('#corpOrder1Accept').show();
                //     $('#order1loading').hide();
                //     location.reload();
                // });
                $(".modal-body p").html("Server Error");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', function() {location.reload()});
                $('#alterDialog').modal('show');
                $('#corpOrder1Accept').show();
                $('#order1loading').hide();
            } else { // All ok
                // swal({
                //     title: "",
                //     text: 'Order Accepted',
                //     type: "success",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                // function () {
                //     // location.reload();
                //     $('#corpOrder1Accept').show();
                //     $('#order1loading').hide();
                //     loadCorpOrders();
                //     window.scrollTo(0,0);
                //     // loadOrderData(openOrders[0].orderNumber);
                // });
                $(".modal-body p").html("Order Accepted");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click',function() {
                    $('#alterDialog').modal('hide');
                    loadCorpOrders();
                    window.scrollTo(0,0);
                });
                $('#alterDialog').modal('show')
                $('#corpOrder1Accept').show();
                $('#order1loading').hide();
            }
        }
    });
}

function logout() {
    localStorage.removeItem("userId");
    window.location = 'login';
}

//Reject Order
$("#order1Reject").click(function () {
    // status = 3; //Order Rejected
    // var order_id = $("#order1Reject").attr("data-order_id");
    // $('#order1Reject').hide();
    // $('#order1rejectloading').show();
    $(".modal-body p").html("Reject " + $("#corpOrder1Accept").attr("data-user") + "'s Order ?");
    $('#confirm').on('click', order1Reject);
    $('#alterDialog').modal('show')
    // swal({
    //     title: "",
    //     text: "Reject " + $("#corpOrder1Accept").attr("data-user") + "'s Order ?",
    //     showCancelButton: true,
    //     confirmButtonClass: "btn-primary",
    //     confirmButtonText: "Reject",
    //     closeOnConfirm: true
    // },
    //     function (result) {
    //         if (result) {
    //             $.ajax({
    //                 url: order_url,
    //                 data: {
    //                     order_id: order_id,
    //                     action: "release",
    //                     access_token: access_token
    //                 },
    //                 type: "POST",
    //                 success: function (response) {
    //                     if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
    //                         // swal({
    //                         //     title: "",
    //                         //     text: errorMessage,
    //                         //     type: "error",
    //                         //     showCancelButton: false,
    //                         //     confirmButtonClass: "btn-danger",
    //                         //     confirmButtonText: "Ok",
    //                         //     closeOnConfirm: false
    //                         // },
    //                         //     function () {
    //                         //         localStorage.removeItem("userId");
    //                         //         window.location = 'login';
    //                         //         // location.reload();
    //                         //     });
    //                         $(".modal-body p").html("Session Expired, Please login agan!");
    //                         $('#confirm').show();
    //                         $('#cancel').hide();
    //                         $('#confirm').off('click').on('click', logout);
    //                         $('#alterDialog').modal('show');
    //                         $('#order1Reject').show();
    //                         $('#order1rejectloading').hide();
    //                     } else if (response.status == 'error') { // card unauthenticated
    //                         // swal({
    //                         //     title: "",
    //                         //     text: response.message,
    //                         //     type: "error",
    //                         //     showCancelButton: false,
    //                         //     confirmButtonClass: "btn-danger",
    //                         //     confirmButtonText: "Ok",
    //                         //     closeOnConfirm: true
    //                         // },
    //                         //     function () {
    //                         //         $('#order1Reject').show();
    //                         //         $('#order1rejectloading').hide();
    //                         //         location.reload();
    //                         //     });
    //                         $(".modal-body p").html("Server Error");
    //                         $('#confirm').show();
    //                         $('#cancel').hide();
    //                         $('#confirm').off('click').on('click', function() {location.reload()});
    //                         $('#alterDialog').modal('show');
    //                         $('#order1Reject').show();
    //                         $('#order1rejectloading').hide();
    //                     } else {
    //                         // swal({
    //                         //     title: "",
    //                         //     text: "Order Rejected !",
    //                         //     type: "success",
    //                         //     showCancelButton: false,
    //                         //     confirmButtonClass: "btn-danger",
    //                         //     confirmButtonText: "Ok",
    //                         //     closeOnConfirm: true
    //                         // },
    //                         //     function () {
    //                         //         $('#order1Reject').show();
    //                         //         $('#order1rejectloading').hide();
    //                         //         // loadOrderData(openOrders[0].orderNumber);
    //                         //         // location.reload();
    //                         //         var script = document.createElement('script');
    //                         //         script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
    //                         //         // script.text = "console.log("+order_id+");"
    //                         //         document.body.appendChild(script);
    //                         //         loadCorpOrders();
    //                         //         window.scrollTo(0,0);
                                    
    //                         //     });
    //                         $(".modal-body p").html("Order Accepted");
    //                         $('#confirm').show();
    //                         $('#cancel').hide();
    //                         $('#confirm').off('click').on('click',function() {
    //                             $('#alterDialog').modal('hide');
    //                         });
    //                         $('#alterDialog').modal('show');
    //                         $('#order1Reject').show();
    //                         $('#order1rejectloading').hide();
    //                     }
    //                 }
    //             });
    //         } else {
    //             $('#order1Reject').show();
    //             $('#order1rejectloading').hide();
    //         }
    //     });
});

function order1Reject() {
    status = 3; //Order Rejected
    var order_id = $("#order1Reject").attr("data-order_id");
    $('#order1Reject').hide();
    $('#order1rejectloading').show();
    $.ajax({
        url: order_url,
        data: {
            order_id: order_id,
            action: "release",
            access_token: access_token
        },
        type: "POST",
        success: function (response) {
            if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
                // swal({
                //     title: "",
                //     text: errorMessage,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: false
                // },
                //     function () {
                //         localStorage.removeItem("userId");
                //         window.location = 'login';
                //         // location.reload();
                //     });
                $(".modal-body p").html("Session Expired, Please login agan!");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', logout);
                $('#alterDialog').modal('show');
                $('#order1Reject').show();
                $('#order1rejectloading').hide();
            } else if (response.status == 'error') { // card unauthenticated
                // swal({
                //     title: "",
                //     text: response.message,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                //     function () {
                //         $('#order1Reject').show();
                //         $('#order1rejectloading').hide();
                //         location.reload();
                //     });
                $(".modal-body p").html("Server Error");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', function() {location.reload()});
                $('#alterDialog').modal('show');
                $('#order1Reject').show();
                $('#order1rejectloading').hide();
            } else {
                // swal({
                //     title: "",
                //     text: "Order Rejected !",
                //     type: "success",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                //     function () {
                //         $('#order1Reject').show();
                //         $('#order1rejectloading').hide();
                //         // loadOrderData(openOrders[0].orderNumber);
                //         // location.reload();
                //         var script = document.createElement('script');
                //         script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
                //         // script.text = "console.log("+order_id+");"
                //         document.body.appendChild(script);
                //         loadCorpOrders();
                //         window.scrollTo(0,0);
                        
                //     });
                $(".modal-body p").html("Order Rejected !");
                $('#confirm').show();
                $('#cancel').hide();
                var script = document.createElement('script');
                script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
                // script.text = "console.log("+order_id+");"
                document.body.appendChild(script);
                $('#confirm').off('click').on('click',function() {
                    $('#alterDialog').modal('hide');
                    loadCorpOrders();
                    window.scrollTo(0,0);
                });
                $('#alterDialog').modal('show');
                $('#order1Reject').show();
                $('#order1rejectloading').hide();
            }
        }
    });
}

$("#order1Save").click(function() {
    // console.log('New Order ', newOrderData.totalAmount);
    // console.log('Old Order ', oldOrderData.totalAmount);
    $('#order1Save').hide();
    $('#order1loading').show();
    if (newOrderData.totalAmount <= oldOrderData.originalTotalAmount) {
        // console.log('Order Data is amount is verified');
        $.ajax({
            url: base_url + 'orders/' + newOrderData.id,
            type: "PATCH",
            data: {
                discountApplied: newOrderData.discountApplied,
                totalAmount: newOrderData.totalAmount,
                tax: newOrderData.tax,
                qty: newOrderData.qty,
                // originalTotalAmount: oldOrderData.totalAmount,
                message: "Order updated successfully",
                orderUpdated: true
            },
            success: function (response) {
                // console.log(response);

                if (newOrderData.removeID.length > 0) {
                    $.each(newOrderData.removeID, function (i, id) {
                        $.ajax({
                            url: base_url + 'orderdetails/' + id,
                            type: 'DELETE',
                            success: function(data) {
                                // console.log('deleted successfully');
                                if (i === newOrderData.removeID.length - 1) {
                                    updateToServer(1);
                                }
                            },
                            error: function(err) {
                                // console.log('Error deleting');
                                alert('Server error, try again.');
                                $('#order1Save').show();
                                $('#order1loading').hide();
                            }
                        })
                    });
                } else {
                    updateToServer(1);
                }
                
            },
            error: function (error) {
                $(".modal-body p").html("Error Saving Order, Try Again");
                $('#confirm').on('click', function() {
                    $('#order1Save').show();
                    $('#order1loading').hide();
                });
                $('#cancel').hide();
                $('#alterDialog').modal('show')
                // swal({
                //     title: '',
                //     text: 'Error Saving Order, Try Again.',
                //     type: 'error',
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: 'Ok',
                //     closeOnConfirm: true
                // }, function() {
                //     $('#order1Save').show();
                //     $('#order1loading').hide();
                // });
            }
        });
    } else {
        $(".modal-body p").html("Order Amount cannot be more than $" + oldOrderData.originalTotalAmount);
        $('#confirm').on('click', function() {
            $('#order1Save').show();
            $('#order1loading').hide();
        });
        $('#cancel').hide();
        $('#alterDialog').modal('show')
        // swal({
        //     title: '',
        //     text: 'Order Amount cannot be more than $' + oldOrderData.originalTotalAmount,
        //     type: 'error',
        //     showCancelButton: false,
        //     confirmButtonClass: 'btn-danger',
        //     confirmButtonText: 'Ok',
        //     closeOnConfirm: true,
        // }, function() {
        //     $('#order1Save').show();
        //     $('#order1loading').hide();
        // })
    }
});

/// Ready to pickup >  Order Picked Up
$("#corpOrder2PickedUpBtn").click(function () {
    var order_id = $("#corpOrder2PickedUpBtn").attr("data-order_id");
    $('#corpOrder2PickedUpBtn').hide();
    $('#order2loading').show();
    $.ajax({
        url: base_url + 'orders/' + order_id,
        data: {
            status: 2
        },
        type: "PATCH",
        success: function (response) {
            if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
                $(".modal-body p").html(errorMessage);
                $('#confirm').on('click', function() {
                    $('#failureAnimation').hide();
                    localStorage.removeItem("userId");
                    window.location = 'login';
                });
                $('#failureAnimation').show();
                $('#cancel').hide();
                $('#alterDialog').modal('show');
                // swal({
                //     title: "",
                //     text: errorMessage,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: false
                // },
                // function () {
                //     localStorage.removeItem("userId");
                //     window.location = 'login';
                //     // location.reload();
                // });
            } else if (response.status == 'error') { // card unauthenticated
                $(".modal-body p").html(response.message);
                $('#confirm').on('click', function() {
                    $('#failureAnimation').hide();
                    $('#corpOrder2PickedUpBtn').show();
                    $('#order2loading').hide();
                    location.reload();
                });
                $('#cancel').hide();
                $('#failureAnimation').show();
                $('#alterDialog').modal('show');
                // swal({
                //     title: "",
                //     text: response.message,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                //     function () {
                //         $('#corpOrder2PickedUpBtn').show();
                //         $('#order2loading').hide();
                //         location.reload();
                //     });
            } else {
                $(".modal-body p").html('Order Delivered!');
                $('#confirm').on('click', function() {
                    $('#successAnimation').hide();
                    $('#corpOrder2PickedUpBtn').show();
                    $('#order2loading').hide();
                    loadCorpOrders();
                    window.scrollTo(0,0);
                });
                $('#cancel').hide();
                $('#successAnimation').show();
                $('#alterDialog').modal('show');
                // swal({
                //     title: "",
                //     text: "Order Delivered!",
                //     type: "success",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                // function () {
                //     $('#corpOrder2PickedUpBtn').show();
                //     $('#order2loading').hide();
                //     // location.reload();
                //     // loadOrderData(readyToPickup[0].orderNumber);
                //     loadCorpOrders();
                //     window.scrollTo(0,0);
                    
                // });
            }
            // alert("Order Picked Up !");
            // location.reload();
        }
    });
});

/// Ready to pickup > Order Reject
$("#corpOrder2RejectBtn").click(function () {
    $(".modal-body p").html("Reject " + $("#corpOrder2RejectBtn").attr("data-user") + "'s Order ?");
    $('#confirm').on('click', corpOrder2RejectBtn);
    $('#alterDialog').modal('show')
    // var order_id = $("#corpOrder2RejectBtn").attr("data-order_id");
    // $('#corpOrder2RejectBtn').hide();
    // $('#order2rejectloading').show();
    // swal({
    //     title: "",
    //     text: "Reject " + $("#corpOrder2RejectBtn").attr("data-user") + "'s Order ?",
    //     showCancelButton: true,
    //     confirmButtonClass: "btn-primary",
    //     confirmButtonText: "Reject",
    //     closeOnConfirm: true
    // },
    //     function (result) {
    //         if (result) {
    //             $.ajax({
    //                 url: order_url,
    //                 data: {
    //                     order_id: order_id,
    //                     action: "reject",
    //                     access_token: access_token
    //                 },
    //                 type: "POST",
    //                 success: function (response) {
    //                     if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
    //                         swal({
    //                             title: "",
    //                             text: errorMessage,
    //                             type: "error",
    //                             showCancelButton: false,
    //                             confirmButtonClass: "btn-danger",
    //                             confirmButtonText: "Ok",
    //                             closeOnConfirm: false
    //                         },
    //                             function () {
    //                                 localStorage.removeItem("userId");
    //                                 window.location = 'login';
    //                                 // location.reload();
    //                             });
    //                     } else if (response.status == 'error') { // card unauthenticated
    //                         swal({
    //                             title: "",
    //                             text: response.message,
    //                             type: "error",
    //                             showCancelButton: false,
    //                             confirmButtonClass: "btn-danger",
    //                             confirmButtonText: "Ok",
    //                             closeOnConfirm: true
    //                         },
    //                             function () {
    //                                 $('#corpOrder2RejectBtn').show();
    //                                 $('#order2rejectloading').hide();
    //                                 location.reload();
    //                             });
    //                     } else {
    //                         swal({
    //                             title: "",
    //                             text: "Order Rejected !",
    //                             type: "success",
    //                             showCancelButton: false,
    //                             confirmButtonClass: "btn-danger",
    //                             confirmButtonText: "Ok",
    //                             closeOnConfirm: true
    //                         },
    //                         function () {
    //                             // location.reload();
    //                             $('#corpOrder2RejectBtn').show();
    //                             $('#order2rejectloading').hide();
    //                             var script = document.createElement('script');
    //                             script.id = "ga_refund_event";
    //                             script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
    //                             document.body.appendChild(script);
    //                             // loadCorpOrders();
    //                             createOrderRejectedRefundNotification(order_id);
    //                             window.scrollTo(0,0);
                                
    //                             // loadOrderData(openOrders[0].orderNumber);
    //                         });
    //                         // swal({
    //                         //     title: "",
    //                         //     text: "Order Rejected !",
    //                         //     type: "success",
    //                         //     showCancelButton: false,
    //                         //     confirmButtonClass: "btn-danger",
    //                         //     confirmButtonText: "Ok",
    //                         //     closeOnConfirm: true
    //                         // },
    //                         //     function () {
    //                         //         // location.reload();
    //                         //         // loadOrderData(readyToPickup[0].orderNumber);
                                    
    //                         //         var script = document.createElement('script');
    //                         //         script.id = "ga_refund_event";
    //                         //         script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
    //                         //         document.body.appendChild(script);
    //                         //         // document.body.removeChild(script);
    //                         //         loadCorpOrders();
    //                         //         window.scrollTo(0,0);
    //                         //     });
    //                     }
    //                     //alert("Order Rejected !");
    
    //                     //location.reload();
    //                 },
    //                 error: function (error) {
    //                     let errorMsg = JSON.parse(error.responseText).raw.message;
    //                     swal({
    //                         title: "",
    //                         text: errorMsg,
    //                         type: "error",
    //                         showCancelButton: false,
    //                         confirmButtonClass: "btn-danger",
    //                         confirmButtonText: "Ok",
    //                         closeOnConfirm: true
    //                     },
    //                     function () {
    //                         $('#corpOrder2RejectBtn').show();
    //                         $('#order2rejectloading').hide();
    //                         var script = document.createElement('script');
    //                         script.id = "ga_refund_event";
    //                         script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
    //                         document.body.appendChild(script);
    //                         // loadCorpOrders();
    //                         // createOrderRejectedRefundNotification(order_id);
    //                         window.scrollTo(0,0);
    //                     });
    //                 }
    //             });
    //         } else {
    //             $('#corpOrder2RejectBtn').show();
    //             $('#order2rejectloading').hide();
    //         }
            
    //     });
});

function corpOrder2RejectBtn() {
    var order_id = $("#corpOrder2RejectBtn").attr("data-order_id");
    $('#corpOrder2RejectBtn').hide();
    $('#order2rejectloading').show();
    $('#alterDialog').modal('hide');
    $.ajax({
        url: order_url,
        data: {
            order_id: order_id,
            action: "reject",
            access_token: access_token
        },
        type: "POST",
        success: function (response) {
            if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
                // swal({
                //     title: "",
                //     text: errorMessage,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: false
                // },
                // function () {
                //     localStorage.removeItem("userId");
                //     window.location = 'login';
                //     // location.reload();
                // });
                $(".modal-body p").html("Session Expired, Please login agan!");
                $('#corpOrder2RejectBtn').show();
                $('#order2rejectloading').hide();
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', logout);
                $('#alterDialog').modal('show');
            } else if (response.status == 'error') { // card unauthenticated
                // swal({
                //     title: "",
                //     text: response.message,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                // function () {
                //     $('#corpOrder2RejectBtn').show();
                //     $('#order2rejectloading').hide();
                //     location.reload();
                // });
                $(".modal-body p").html("Server Error");
                $('#corpOrder2RejectBtn').show();
                $('#order2rejectloading').hide();
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', function() {location.reload()});
                $('#alterDialog').modal('show')
            } else {
                // swal({
                //     title: "",
                //     text: "Order Rejected !",
                //     type: "success",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                // function () {
                //     // location.reload();
                //     $('#corpOrder2RejectBtn').show();
                //     $('#order2rejectloading').hide();
                //     var script = document.createElement('script');
                //     script.id = "ga_refund_event";
                //     script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
                //     document.body.appendChild(script);
                //     // loadCorpOrders();
                //     createOrderRejectedRefundNotification(order_id);
                //     window.scrollTo(0,0);
                    
                //     // loadOrderData(openOrders[0].orderNumber);
                // });
                $(".modal-body p").html("Order Rejected");
                var script = document.createElement('script');
                script.id = "ga_refund_event";
                script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
                document.body.appendChild(script);
                $('#confirm').off('click').on('click',function() {
                    $('#alterDialog').modal('hide');
                    createOrderRejectedRefundNotification(order_id);
                    window.scrollTo(0,0);
                });
                $('#alterDialog').modal('show');
                $('#corpOrder2RejectBtn').show();
                $('#order2rejectloading').hide();
            }
            //alert("Order Rejected !");

            //location.reload();
        },
        error: function (error) {
            let errorMsg = JSON.parse(error.responseText).raw.message;
            // swal({
            //     title: "",
            //     text: errorMsg,
            //     type: "error",
            //     showCancelButton: false,
            //     confirmButtonClass: "btn-danger",
            //     confirmButtonText: "Ok",
            //     closeOnConfirm: true
            // },
            // function () {
            //     $('#corpOrder2RejectBtn').show();
            //     $('#order2rejectloading').hide();
            //     var script = document.createElement('script');
            //     script.id = "ga_refund_event";
            //     script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
            //     document.body.appendChild(script);
            //     // loadCorpOrders();
            //     // createOrderRejectedRefundNotification(order_id);
            //     window.scrollTo(0,0);
            // });
            $(".modal-body p").html(errorMsg);
            $('#confirm').show();
            $('#cancel').hide();
            $('#confirm').off('click').on('click', function() {location.reload()});
            $('#alterDialog').modal('show');
            $('#corpOrder2RejectBtn').show();
            $('#order2rejectloading').hide();
        }
    });
}

$('#order2Save').click(function() {
    // console.log('New Order ', newOrderData.totalAmount);
    // console.log('Old Order ', oldOrderData.totalAmount);
    $('#order2Save').hide();
    $('#order2loading').show();
    if (newOrderData.totalAmount <= oldOrderData.originalTotalAmount) {
        // console.log('Update Order');
        $.ajax({
            url: base_url + 'orders/' + newOrderData.id,
            type: "PATCH",
            data: {
                discountApplied: newOrderData.discountApplied,
                totalAmount: newOrderData.totalAmount,
                tax: newOrderData.tax,
                qty: newOrderData.qty,
                // originalTotalAmount: oldOrderData.totalAmount,
                message: newOrderData.totalAmount === oldOrderData.totalAmount ? "Order updated after order is accepted." : "Order updated and initiated a refund.",
                orderUpdated: true
            },
            success: function (response) {
                // console.log(response);

                if (newOrderData.removeID.length > 0) {
                    $.each(newOrderData.removeID, function (i, id) {
                        $.ajax({
                            url: base_url + 'orderdetails/' + id,
                            type: 'DELETE',
                            success: function(data) {
                                // console.log('deleted successfully');
                                if (i === newOrderData.removeID.length - 1) {
                                    $.ajax({
                                        url: order_url,
                                        type: "POST",
                                        data: {
                                            order_id: newOrderData.id,
                                            action: "partial-refund",
                                            access_token: access_token
                                        },
                                        success: function (refund) {
                                            if (refund.status === 'success') {
                                                // swal({
                                                //     title: '',
                                                //     text: newOrderData.totalAmount === oldOrderData.totalAmount ? "Order updated after order is accepted." : 'Order Saved, refund initiated of $'+refund.refundAmt,
                                                //     showCancelButton: false,
                                                //     confirmButtonClass: 'btn-success',
                                                //     confirmButtonText: 'OK',
                                                //     closeOnConfirm: true,
                                                // }, function() {
                                                //     updateToServer(2, refund.refundAmt);
                                                // })
                                                $(".modal-body p").html(newOrderData.totalAmount === oldOrderData.totalAmount ? "Order updated after order is accepted." : 'Order Saved, refund initiated of $'+refund.refundAmt);
                                                $('#confirm').show();
                                                $('#cancel').hide();
                                                $('#confirm').off('click').on('click', function() {updateToServer(2, refund.refundAmt)});
                                                $('#alterDialog').modal('show');
                                                $('#order2Save').show();
                                                $('#order2loading').hide();
                                            } else {
                                                // swal({
                                                //     title: '',
                                                //     text: 'Order save error, initiate refund manually of $'+refund.refundAmt,
                                                //     showCancelButton: false,
                                                //     confirmButtonClass: 'btn-success',
                                                //     confirmButtonText: 'OK',
                                                //     closeOnConfirm: true,
                                                // }, function() {
                                                //     // $('#order2editText').click();
                                                //     $('#order2loading').hide();
                                                //     $('#order2Save').show();
                                                // })
                                                $(".modal-body p").html('Order save error, initiate refund manually of $'+refund.refundAmt);
                                                $('#confirm').show();
                                                $('#cancel').hide();
                                                $('#confirm').off('click').on('click', function() {updateToServer(2, refund.refundAmt)});
                                                $('#alterDialog').modal('show');
                                                $('#order2Save').show();
                                                $('#order2loading').hide();
                                            }
                                        },
                                        error : function (error) {
                                            console.log(error);
                                            // $('#order2editText').click();
                                            $('#order2loading').hide();
                                            $('#order2Save').show();
                                        }
                                    })
                                    // updateToServer();
                                }
                            },
                            error: function(err) {
                                console.log('Error deleting');
                                alert('Server error, try again.');
                                $('#order2Save').show();
                                $('#order2loading').hide();
                            }
                        })
                    });
                } else {
                    $.ajax({
                        url: order_url,
                        type: "POST",
                        data: {
                            order_id: newOrderData.id,
                            action: "partial-refund",
                            access_token: access_token
                        },
                        success: function (refund) {
                            if (refund.status === 'success') {
                                // swal({
                                //     title: '',
                                //     text: newOrderData.totalAmount === oldOrderData.totalAmount ? "Order updated after order is accepted." : 'Order Save, refund initiated of $'+refund.refundAmt,
                                //     showCancelButton: false,
                                //     confirmButtonClass: 'btn-success',
                                //     confirmButtonText: 'OK',
                                //     closeOnConfirm: true,
                                // }, function() {
                                //     updateToServer(2, refund.refundAmt);
                                // })
                                $(".modal-body p").html(newOrderData.totalAmount === oldOrderData.totalAmount ? "Order updated after order is accepted." : 'Order Save, refund initiated of $'+refund.refundAmt);
                                $('#confirm').show();
                                $('#cancel').hide();
                                $('#confirm').off('click').on('click', function() {updateToServer(2, refund.refundAmt)});
                                $('#alterDialog').modal('show');
                                $('#order2Save').show();
                                $('#order2loading').hide();
                            } else {
                                // swal({
                                //     title: '',
                                //     text: 'Order save error, initiate refund manually of $'+refund.refundAmt,
                                //     showCancelButton: false,
                                //     confirmButtonClass: 'btn-success',
                                //     confirmButtonText: 'OK',
                                //     closeOnConfirm: true,
                                // }, function() {
                                //     // $('#order2editText').click();
                                //     $('#order2Save').show();
                                //     $('#order2loading').hide();
                                // })
                                $(".modal-body p").html('Order save error, initiate refund manually of $'+refund.refundAmt);
                                $('#confirm').show();
                                $('#cancel').hide();
                                $('#confirm').off('click').on('click', function() {});
                                $('#alterDialog').modal('show');
                                $('#order2Save').show();
                                $('#order2loading').hide();
                            }
                        },
                        error : function (error) {
                            console.log(error);
                            // $('#order2editText').click();
                            $('#order2Save').show();
                            $('#order2loading').hide();
                        }
                    })
                }
                
            },
            error: function (error) {
                // swal({
                //     title: '',
                //     text: 'Error Saving Order, Try Again.',
                //     type: 'error',
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: 'Ok',
                //     closeOnConfirm: true
                // }, function() {});
                $(".modal-body p").html('Error Saving Order, Try Again.');
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', function() {});
                $('#alterDialog').modal('show');
                $('#order2Save').show();
                $('#order2loading').hide();
            }
        });
    } else {
        // swal({
        //     title: '',
        //     text: 'Order Amount cannot be more than $' + oldOrderData.originalTotalAmount,
        //     type: 'error',
        //     showCancelButton: false,
        //     confirmButtonClass: 'btn-danger',
        //     confirmButtonText: 'Ok',
        //     closeOnConfirm: true,
        // }, function() {
        //     $('#order2Save').show();
        //     $('#order2loading').hide();
        // })
        $(".modal-body p").html('Order Amount cannot be more than $' + oldOrderData.originalTotalAmount);
        $('#confirm').show();
        $('#cancel').hide();
        $('#confirm').off('click').on('click', function() {});
        $('#alterDialog').modal('show');
        $('#order2Save').show();
        $('#order2loading').hide();
    }
});

/// Ready to pickup > Order Reject
$("#order5RejectBtn").click(function () {
    $(".modal-body p").html("Reject " + $("#order5RejectBtn").attr("data-user") + "'s Order ?");
    $('#confirm').on('click', order5RejectBtn);
    $('#cancel').show();
    $('#alterDialog').modal('show');
    // var order_id = $("#order5RejectBtn").attr("data-order_id");
    // $('#order5RejectBtn').hide();
    // $('#order5rejectloading').show();
    // swal({
    //     title: "",
    //     text: "Reject " + $("#order5RejectBtn").attr("data-user") + "'s Order ?",
    //     showCancelButton: true,
    //     confirmButtonClass: "btn-primary",
    //     confirmButtonText: "Reject",
    //     closeOnConfirm: true
    // },
    // function (result) { 
    //     if (result) {
    //         $.ajax({
    //             url: order_url,
    //             data: {
    //                 order_id: order_id,
    //                 action: "reject",
    //                 access_token: access_token
    //             },
    //             type: "POST",
    //             success: function (response) {
    //                 if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
    //                     swal({
    //                         title: "",
    //                         text: errorMessage,
    //                         type: "error",
    //                         showCancelButton: false,
    //                         confirmButtonClass: "btn-danger",
    //                         confirmButtonText: "Ok",
    //                         closeOnConfirm: false
    //                     },
    //                         function () {
    //                             localStorage.removeItem("userId");
    //                             window.location = 'login';
    //                             // location.reload();
    //                         });
    //                 } else if (response.status == 'error') { // card unauthenticated
    //                     swal({
    //                         title: "",
    //                         text: response.message,
    //                         type: "error",
    //                         showCancelButton: false,
    //                         confirmButtonClass: "btn-danger",
    //                         confirmButtonText: "Ok",
    //                         closeOnConfirm: true
    //                     },
    //                         function () {
    //                             $('#order5RejectBtn').show();
    //                             $('#order5rejectloading').hide();
    //                             location.reload();
    //                         });
    //                 } else {
    //                     swal({
    //                         title: "",
    //                         text: "Order Rejected !",
    //                         type: "success",
    //                         showCancelButton: false,
    //                         confirmButtonClass: "btn-danger",
    //                         confirmButtonText: "Ok",
    //                         closeOnConfirm: true
    //                     },
    //                     function () {
    //                         $('#order5RejectBtn').show();
    //                         $('#order5rejectloading').hide();
    //                         // location.reload();
    //                         var script = document.createElement('script');
    //                         script.id = "ga_refund_event";
    //                         script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
    //                         document.body.appendChild(script);
    //                         // loadCorpOrders();
    //                         createOrderRejectedRefundNotification(order_id);
    //                         window.scrollTo(0,0);
                            
    //                         // loadOrderData(openOrders[0].orderNumber);
    //                     });
    //                     // swal({
    //                     //     title: "",
    //                     //     text: "Order Rejected !",
    //                     //     type: "success",
    //                     //     showCancelButton: false,
    //                     //     confirmButtonClass: "btn-danger",
    //                     //     confirmButtonText: "Ok",
    //                     //     closeOnConfirm: true
    //                     // },
    //                     //     function () {
    //                     //         // location.reload();
    //                     //         // loadOrderData(readyToPickup[0].orderNumber);
                                
    //                     //         var script = document.createElement('script');
    //                     //         script.id = "ga_refund_event";
    //                     //         script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
    //                     //         document.body.appendChild(script);
    //                     //         // document.body.removeChild(script);
    //                     //         loadCorpOrders();
    //                     //         window.scrollTo(0,0);
    //                     //     });
    //                 }
    //                 //alert("Order Rejected !");

    //                 //location.reload();
    //             },
    //             error : function (error) {
                    
    //                 swal({
    //                     title: '',
    //                     text: 'Error Rejecting, initiate refund manually',
    //                     showCancelButton: false,
    //                     confirmButtonClass: 'btn-success',
    //                     confirmButtonText: 'OK',
    //                     closeOnConfirm: true,
    //                 }, function() {
    //                     // $('#order5editText').click();
    //                     $('#order5RejectBtn').show();
    //                     $('#order5rejectloading').hide();
    //                 })
    //             }
    //         });
    //     } else {
    //         $('#order5RejectBtn').show();
    //         $('#order5rejectloading').hide();
    //     }
        
    // });
});

function order5RejectBtn() {
    var order_id = $("#order5RejectBtn").attr("data-order_id");
    $('#order5RejectBtn').hide();
    $('#order5rejectloading').show();
    $.ajax({
        url: order_url,
        data: {
            order_id: order_id,
            action: "reject",
            access_token: access_token
        },
        type: "POST",
        success: function (response) {
            if (response.status_code == 'SESSION_EXPIRED') { //Invalid access token
                // swal({
                //     title: "",
                //     text: errorMessage,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: false
                // },
                //     function () {
                //         localStorage.removeItem("userId");
                //         window.location = 'login';
                //         // location.reload();
                //     });
                $(".modal-body p").html("Session Expired, Please login agan!");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', logout);
                $('#alterDialog').modal('show');
                $('#order5RejectBtn').hide();
                $('#order5rejectloading').show();
            } else if (response.status == 'error') { // card unauthenticated
                // swal({
                //     title: "",
                //     text: response.message,
                //     type: "error",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                // function () {
                //     $('#order5RejectBtn').show();
                //     $('#order5rejectloading').hide();
                //     location.reload();
                // });
                $(".modal-body p").html("Server Error");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', function() {location.reload()});
                $('#alterDialog').modal('show');
                $('#order5RejectBtn').hide();
                $('#order5rejectloading').show();
            } else {
                // swal({
                //     title: "",
                //     text: "Order Rejected !",
                //     type: "success",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                // function () {
                //     $('#order5RejectBtn').show();
                //     $('#order5rejectloading').hide();
                //     // location.reload();
                //     var script = document.createElement('script');
                //     script.id = "ga_refund_event";
                //     script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
                //     document.body.appendChild(script);
                //     // loadCorpOrders();
                //     createOrderRejectedRefundNotification(order_id);
                //     window.scrollTo(0,0);
                    
                //     // loadOrderData(openOrders[0].orderNumber);
                // });
                $(".modal-body p").html("Order Rejected !");
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click',function() {
                    $('#alterDialog').modal('hide');
                    createOrderRejectedRefundNotification(order_id);
                    window.scrollTo(0,0);
                });
                $('#alterDialog').modal('show')
                $('#order5RejectBtn').show();
                $('#order5rejectloading').hide();
                // swal({
                //     title: "",
                //     text: "Order Rejected !",
                //     type: "success",
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: "Ok",
                //     closeOnConfirm: true
                // },
                //     function () {
                //         // location.reload();
                //         // loadOrderData(readyToPickup[0].orderNumber);
                        
                //         var script = document.createElement('script');
                //         script.id = "ga_refund_event";
                //         script.text = "gtag('event', 'refund', { \"transaction_id\": '" + order_id + "' })";
                //         document.body.appendChild(script);
                //         // document.body.removeChild(script);
                //         loadCorpOrders();
                //         window.scrollTo(0,0);
                //     });
            }
            //alert("Order Rejected !");

            //location.reload();
        },
        error : function (error) {
            
            // swal({
            //     title: '',
            //     text: 'Error Rejecting, initiate refund manually',
            //     showCancelButton: false,
            //     confirmButtonClass: 'btn-success',
            //     confirmButtonText: 'OK',
            //     closeOnConfirm: true,
            // }, function() {
            //     // $('#order5editText').click();
            //     $('#order5RejectBtn').show();
            //     $('#order5rejectloading').hide();
            // })
            $(".modal-body p").html('Error Rejecting, initiate refund manually');
            $('#confirm').show();
            $('#cancel').hide();
            $('#confirm').off('click').on('click',function() {});
            $('#alterDialog').modal('show')
            $('#order5RejectBtn').show();
            $('#order5rejectloading').hide();
        }
    });
}

$('#order5Save').click(function() {
    console.log('New Order ', newOrderData.totalAmount);
    console.log('Old Order ', oldOrderData.totalAmount);
    $('#order5loading').show();
    $('#order5Save').hide();
    if (newOrderData.totalAmount <= oldOrderData.originalTotalAmount) {
        console.log('Update Order');
        $.ajax({
            url: base_url + 'orders/' + newOrderData.id,
            type: "PATCH",
            data: {
                discountApplied: newOrderData.discountApplied,
                totalAmount: newOrderData.totalAmount,
                tax: newOrderData.tax,
                qty: newOrderData.qty,
                // originalTotalAmount: oldOrderData.totalAmount,
                message: newOrderData.totalAmount === oldOrderData.totalAmount ? "Order updated after order is delivered." : "Order updated and initiated a refund.",
                orderUpdated: true
            },
            success: function (response) {
                console.log(response);

                if (newOrderData.removeID.length > 0) {
                    $.each(newOrderData.removeID, function (i, id) {
                        $.ajax({
                            url: base_url + 'orderdetails/' + id,
                            type: 'DELETE',
                            success: function(data) {
                                console.log('deleted successfully');
                                if (i === newOrderData.removeID.length - 1) {
                                    $.ajax({
                                        url: order_url,
                                        type: "POST",
                                        data: {
                                            order_id: newOrderData.id,
                                            action: "partial-refund",
                                            access_token: access_token
                                        },
                                        success: function (refund) {
                                            if (refund.status === 'success') {
                                                // swal({
                                                //     title: '',
                                                //     text: 'Order Saved, refund initiated of $'+refund.refundAmt,
                                                //     showCancelButton: false,
                                                //     confirmButtonClass: 'btn-success',
                                                //     confirmButtonText: 'OK',
                                                //     closeOnConfirm: true,
                                                // }, function() {
                                                //     updateToServer(2, refund.refundAmt);
                                                // })
                                                $(".modal-body p").html('Order Saved, refund initiated of $'+refund.refundAmt);
                                                $('#confirm').show();
                                                $('#cancel').hide();
                                                $('#confirm').off('click').on('click', function() {updateToServer(2, refund.refundAmt)});
                                                $('#alterDialog').modal('show');
                                                $('#order5loading').hide();
                                                $('#order5Save').show();
                                            } else {
                                                // swal({
                                                //     title: '',
                                                //     text: 'Order save error, initiate refund manually of $'+refund.refundAmt,
                                                //     showCancelButton: false,
                                                //     confirmButtonClass: 'btn-success',
                                                //     confirmButtonText: 'OK',
                                                //     closeOnConfirm: true,
                                                // }, function() {
                                                //     // $('#order5editText').click();
                                                //     $('#order5loading').hide();
                                                //     $('#order5Save').show();
                                                // })
                                                $(".modal-body p").html('Order save error, initiate refund manually of $'+refund.refundAmt);
                                                $('#confirm').show();
                                                $('#cancel').hide();
                                                $('#confirm').off('click').on('click', function() {});
                                                $('#alterDialog').modal('show');
                                                $('#order5loading').hide();
                                                $('#order5Save').show();
                                            }
                                        },
                                        error : function (error) {
                                            console.log(error);
                                            // $('#order5editText').click();
                                            $('#order5loading').hide();
                                            $('#order5Save').show();
                                        }
                                    })
                                    // updateToServer();
                                }
                            },
                            error: function(err) {
                                console.log('Error deleting');
                                alert('Server error, try again.');
                                $('#order5Save').show();
                                $('#order5loading').hide();
                            }
                        })
                    });
                } else {
                    $.ajax({
                        url: order_url,
                        type: "POST",
                        data: {
                            order_id: newOrderData.id,
                            action: "partial-refund",
                            access_token: access_token
                        },
                        success: function (refund) {
                            if (refund.status === 'success') {
                                // swal({
                                //     title: '',
                                //     text: 'Order Save, refund initiated of $'+refund.refundAmt,
                                //     showCancelButton: false,
                                //     confirmButtonClass: 'btn-success',
                                //     confirmButtonText: 'OK',
                                //     closeOnConfirm: true,
                                // }, function() {
                                //     updateToServer(2, refund.refundAmt);
                                // })
                                $(".modal-body p").html('Order Saved, refund initiated of $'+refund.refundAmt);
                                $('#confirm').show();
                                $('#cancel').hide();
                                $('#confirm').off('click').on('click', function() {updateToServer(2, refund.refundAmt)});
                                $('#alterDialog').modal('show');
                                $('#order5loading').hide();
                                $('#order5Save').show();
                            } else {
                                // swal({
                                //     title: '',
                                //     text: 'Order save error, initiate refund manually of $'+refund.refundAmt,
                                //     showCancelButton: false,
                                //     confirmButtonClass: 'btn-success',
                                //     confirmButtonText: 'OK',
                                //     closeOnConfirm: true,
                                // }, function() {
                                //     // $('#order5editText').click();
                                //     $('#order5Save').show();
                                //     $('#order5loading').hide();
                                // })
                                $(".modal-body p").html('Order save error, initiate refund manually of $'+refund.refundAmt);
                                $('#confirm').show();
                                $('#cancel').hide();
                                $('#confirm').off('click').on('click', function() {});
                                $('#alterDialog').modal('show');
                                $('#order5loading').hide();
                                $('#order5Save').show();
                            }
                        },
                        error : function (error) {
                            console.log(error);
                            // $('#order5editText').click();
                            $('#order5Save').show();
                            $('#order5loading').hide();
                        }
                    })
                }
                
            },
            error: function (error) {
                // swal({
                //     title: '',
                //     text: 'Error Saving Order, Try Again.',
                //     type: 'error',
                //     showCancelButton: false,
                //     confirmButtonClass: "btn-danger",
                //     confirmButtonText: 'Ok',
                //     closeOnConfirm: true
                // }, function() {});
                $(".modal-body p").html('Error Saving Order, Try Again.');
                $('#confirm').show();
                $('#cancel').hide();
                $('#confirm').off('click').on('click', function() {});
                $('#alterDialog').modal('show');
                $('#order5loading').hide();
                $('#order5Save').show();
            }
        });
    } else {
        // swal({
        //     title: '',
        //     text: 'Order Amount cannot be more than $' + oldOrderData.originalTotalAmount,
        //     type: 'error',
        //     showCancelButton: false,
        //     confirmButtonClass: 'btn-danger',
        //     confirmButtonText: 'Ok',
        //     closeOnConfirm: true,
        // }, function() {
        //     $('#order5Save').show();
        //     $('#order5loading').hide();
        // })
        $(".modal-body p").html('Order Amount cannot be more than $' + oldOrderData.originalTotalAmount);
        $('#confirm').show();
        $('#cancel').hide();
        $('#confirm').off('click').on('click', function() {});
        $('#alterDialog').modal('show');
        $('#order5loading').hide();
        $('#order5Save').show();
    }
})

/* For ordering according to Revenue/Date */
$('input[name=selector]').on('change', function () {
    var filter = $('input[name=selector]:checked').val();

    var $divs = $("#completed tbody tr");
    if (filter === 'Revenue') {
        $("#completed tbody").html('');
        pastOrders = sortOrders(pastOrders, 'totalAmount', true);
        //sort by revenue asceding
        // var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
        //     return $(a).find(".orderRevenue").val() < $(b).find(".orderRevenue").val();
        // });
        // $("#completed tbody").html('');
        // $("#completed tbody").append(alphabeticallyOrderedDivs);
    } else {
        //sort product date
        // var numericallyOrderedDivs = $divs.sort(function (a, b) {
        //     //return $(a).find(".offer .productDate").text() > $(b).find("td.offer td.productDate").text();
        //     return $(a).find(".orderDate").val() > $(b).find(".orderDate").val();
        // });
        $("#completed tbody").html('');
        pastOrders = sortOrders(pastOrders, 'createdAt', false);
        //$("#completed tbody").append(numericallyOrderedDivs);
    }

    $.each(pastOrders, function (i, field) {
        // $("#order4 .card3").css("display", "none");
        if (i == 0) { } else {
            if (field.status === 2) {
                tick = '<i class="fa fa-check" aria-hidden="true"></i>';
                title = 'Order <br/>Completed ';
            } else if (field.status === 3) {
                tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                title = 'Order <br/>Rejected ';
            } else if (field.status === 4) {
                tick = '<i class="fa fa-times" aria-hidden="true"></i>';
                title = 'Order <br/>Cancelled ';
            }
            var orderList =
                '<tr>' +
                '<input class="orderRevenue" type="hidden" value="' + field.createdAt + '">' +
                '<input class="orderDate" type="hidden" value="' + field.totalAmount + '">' +
                '<td>' + tick + '</td>' +
                '<td>' + title + '</td>' +
                '<td>' + moment(field.createdAt).format('MMM DD. ddd') + '</td>' +
                '<td class="orderNumber">' + field.orderNumber + '</td>' +
                '<td>' + moment(field.deliveryTime).format('hh:mm a') + '</td>' +
                '<td>' + moment(field.createdAt).format('hh:mm a') + '</td>' +
                '<td>$' + field.totalAmount.toFixed(2) + '</td>' +
                '</tr>';
            $("#completed tbody").append(orderList);
        }
    });
});

function sortOrders(order, field, asc) {
    order = order.sort(function (a, b) {
        if (asc) {
            return (a[field] > b[field]) ? 1 : ((a[field] < b[field]) ? -1 : 0);
        } else {
            return (b[field] > a[field]) ? 1 : ((b[field] < a[field]) ? -1 : 0);
        }
    });
    return order;
}

if (matchMedia) {
    const mq = window.matchMedia("(min-width: 500px)");
    mq.addListener(WidthChange);
    WidthChange(mq);
}
  
// media query change
function WidthChange(mq) {
    if (mq.matches) {
        // window width is at least 500px
        $('.order-right').css({ transform: 'translate(0%)' });
    } 
}

function saveTracking(id) {
    console.log('id = ', id)
    const carrierName = $('#carrierName-'+id).val().trim()
    const trackingNo = $('#trackingNo-'+id).val().trim()
    console.log(id, carrierName, trackingNo)
    if (id && 
        (carrierName !== '' && carrierName !== undefined) && 
        (trackingNo !== '' && trackingNo !== undefined)) {

        console.log('DO POST')
        $.ajax({
            url: base_url + 'recipientorders/' + id,
            data: {
                carrierName,
                trackingNo
            },
            type: "PATCH",
            success: function (response) {
                console.log(response)
                loadOrders();
            },
            error: function (error) {
                console.log(error)
            }
        })
        
    }
}

function check(num) {
    console.log(num)
}