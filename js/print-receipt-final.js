/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global base_url */
var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}
// $("aside").hide();
$("aside").show();
$("nav").show();
$(".breadcrumb").show();
$(".btn-print").show();

$(document).ready(function () {

});
var params = window.location.search.split('=').pop();
$.ajax({
    url: base_url + 'orders?filter={"include":["account",{"orderdetails":["product"]}],"where":{"orderNumber":"' + params + '"}}',
    success: function (data) {
        data = data[0];
        console.log(data);
        $(".card .receipt-head .receipt-head-inner h3").html(data.qty + ' items for ');
        $(".card .receipt-head p").html('Order Number: ' + data.orderNumber);
        $(".username").html(((typeof data.account != "undefined") ? (data.account.firstName ? data.account.firstName : '') + ' ' + (data.account.lastName ? data.account.lastName : '') : ""));
        $(".address").html(data.address);
        $(".phone").html(data.account.phoneNumber);
        $(".email").html(data.account.email);
        if (data.companyName != undefined && data.companyName != '') {
            $(".companyName").html(data.companyName).removeClass('hidden');
        }
        if (data.aptNumber != undefined && data.aptNumber != '') {
            $(".aptNumber").html(data.aptNumber).removeClass('hidden');
        }
        if (data.deliveryInstructions != "") {
            $(".deliveryInstructions").html("<b>Delivery Instructions:</b> " + data.deliveryInstructions).show();
        } else {
            $(".deliveryInstructions").hide();
        }
        var itemList = '';
        var subtotal = 0;
        var tax = 0;
        var rewards = 0;
        $.each(data.orderdetails, function (key, value) {
            itemList = '<tr>' +
                '<td>' + value.product.name + '<br/><span>' + value.product.category + '</span></td>' +
                '<td>' + value.quantity + '</td>' +
                '<td>$' + (value.price * value.quantity).toFixed(2) + '</td>' +
                //                    '<td>Nov 1. Friday / 2pm</td>' +
                '<td>' + moment(data.deliveryTime).format('MMM D. dddd / ha') + '</td>' +
                '</tr>';
            $(".table-receipt tbody").append(itemList);
            rewards = rewards + ((value.actualPrice - value.price) * value.quantity);  //    (original price - sale price) * qty
            subtotal = parseFloat(subtotal) + parseFloat(value.price * value.quantity);
        });
        if (data.tax != null) {
            tax = data.tax.toFixed(2);
        }
        else {
            tax = '0.0';
        }

        if (data.deliveryFee != null) {
            deliveryFee = data.deliveryFee.toFixed(2);
        }
        else {
            deliveryFee = '0.0';
        }
        console.log("tip," + data.tipAmt)
        if (data.tipAmt != null) {
            tipAmt = data.tipAmt.toFixed(2);
            console.log("tip in ," + data.tipAmt)
        }
        else {
            tipAmt = '0.0';
        }
        $(".subtotal").html('$' + subtotal.toFixed(2));
        $(".tax").html('$' + tax);
        $(".deliveryFee").html('$' + deliveryFee);
        $(".tipAmt").html('$' + tipAmt);
        $(".rewards").html('$' + rewards.toFixed(2));
        $(".paid").html('$' + data.totalAmount.toFixed(2));

        //$(".table-receipt tbody").append(itemList);
        window.print();
        // setTimeout(function () {
        // window.close();
        // }, 10000);
    },
    error: function (response) {
        console.log(response);
        if (response.status == 401) {
            swal({
                title: "",
                text: errorMessage,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: false
            },
                function () {
                    localStorage.removeItem("userId");
                    window.location = 'login';
                });
        }
    }
});
