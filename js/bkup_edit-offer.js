var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}

//Accept number only in input
jQuery('.number').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

$(function () {
    $("#editDatepicker").datepicker({
        dateFormat: 'MM dd, yy'
    });
    $("#editDatepicker1").datepicker({
        dateFormat: 'MM dd, yy'
    });

    $.ajax({
        url: base_url + 'offers/' + params + '?filter={"include":"product"}',
        success: function (data) {
            $("#editProductName").val(data.product.name);
            $("#editOfferName").val(data.name);
            $("#editSkuNumber").val(data.product.sku);
            $("#editUpcCode").val(data.product.upc);
            $("#editCategory").val(data.product.category);
            $("#editOriginalPrice").val(data.originalPrice);
            $("#editDiscountPrice").val(data.salePrice);
            $("#editQuantity").val(data.totalQty);
            $("#editDatepicker").val(moment(data.startsAt).format('MMMM DD, YYYY'));
            $("#editDatepicker1").val(moment(data.endsAt).format('MMMM DD, YYYY'));
            $("#editDescription").val(data.description);
        }
    });
});

$(".save-edit button").click(function () {

    productName = $("#editProductName").val();
    offerName = $("#editOfferName").val();
    sku = $("#editSkuNumber").val();
    upc = $("#editUpcCode").val();
    category = $("#editCategory").find(":selected").val();
    oPrice = $("#editOriginalPrice").val();
    dPrice = $("#editDiscountPrice").val();
    quantity = $("#editQuantity").val();
    start_date = $("#editDatepicker").val();
    end_date = $("#editDatepicker1").val();
    description = $("#editDescription").val();
    offer_id = params;

    var validation = formValidation();
    if (validation) {
        var productData = {
            "name": productName,
            "category": category,
            "upc": upc,
            "sku": sku
        };
        $.ajax({
            type: 'post',
            url: base_url + 'products/upsertWithWhere?where={ "upc" : "' + upc + '", "sku" : "' + sku + '"}',
            data: JSON.stringify(productData),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                $.ajax({
                    type: 'patch',
                    url: base_url + 'offers/' + params,
                    data: JSON.stringify({
                        "establishmentId": localStorage.getItem('establishmentId'),
                        "name": offerName,
                        "description": description,
                        "startsAt": start_date,
                        "endsAt": end_date,
                        "originalPrice": oPrice,
                        "salePrice": dPrice,
                        "totalQty": quantity,
                        "remQty": quantity,
                        "productId": data.id
                    }),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (response) {
                        window.location = 'offers';
                    }
                });
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

function formValidation() {
    var productName = $("#editProductName").val();
    var offerName = $("#editOfferName").val();
    var sku = $("#editSkuNumber").val();
    var upc = $("#editUpcCode").val();
    var category = $("#editCategory").find(":selected").val();
    var oPrice = $("#editOriginalPrice").val();
    var dPrice = $("#editDiscountPrice").val();
    var quantity = $("#editQuantity").val();
    var start_date = $("#editDatepicker").val();
    var end_date = $("#editDatepicker1").val();
    var description = $("#editDescription").val();

    var valid = true;

    if (productName === '') {
        valid = false;
    }
    if (offerName === '') {
        valid = false;
    }
    if (sku === '') {
        valid = false;
    }
    if (upc === '') {
        valid = false;
    }
    if (category === '') {
        valid = false;
    }
    if (oPrice === '') {
        valid = false;
    }
    if (dPrice === '') {
        valid = false;
    }
    if (quantity === '') {
        valid = false;
    }
    if (start_date === '') {
        valid = false;
    }
    if (end_date === '') {
        valid = false;
    }
    if (description === '') {
        valid = false;
    }

    //Validate form
    if (valid === false) {
        return false;
    }
    else {
        return true;
    }
}