/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global base_url */

var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}

$(function () {
    $(".datepicker-head").datepicker({
        dateFormat: 'mm/dd/yy'
    });
});

var past_offers = Array();

$(document).ready(function () {
    sdate = localStorage.getItem("start_date");
    edate = localStorage.getItem("end_date");
    ///If local storage have startdate or end date then it will show old date
    if (sdate == null || edate == null) {
        var startDate = moment().startOf('month').format("MM/DD/YYYY");
        var endDate = moment().format("MM/DD/YYYY");
    } else {
        var startDate = sdate;
        var endDate = edate;
    }
    loadData(startDate, endDate);
});

function loadData(startDate, endDate) {
    $(".startDate").val(startDate);
    $(".endDate").val(endDate);
    $.ajax({
        url: base_url + 'offers/getOffers?establishmentId=' + localStorage.getItem('establishmentId') + '&startDate=' + moment(startDate).format("YYYY-MM-DD") + '&endDate=' + moment(endDate).format("YYYY-MM-DD") + '&active=false',
        //url : 'http://18.221.89.220:3000/api/offers/getOffers?establishmentId=5a24a28910785a5c1aa5467c&startDate=2017-01-01&endDate=2018-12-31&active=true',
        success: function (result) {
            if (result.length === 0) {
                $("#past_offer").html('<tr><td colspan = "4" style="text-align : center" >No Record found !</td></tr>');
            } else {
                past_offers = result;
                $("#past_offer").html('');
                $.each(result, function (i, field) {
                    var row = '<tr class="offer" data-offer-id="' + field.offer.id + '">' +
                        '<td>' + field.offer.product.upc + '</td>' +
                        '<td class="qty">' + field.offer.totalQty + ' items</td>' +
                        '<td class="productRevenue"><a>$' + field.revenue + '</a></td>' +
                        '<td class="productDate">' +
                        '<span>Start:</span> ' + moment(field.offer.startsAt).format("MMM D") +
                        '<br>' +
                        '<span>End:</span> ' + moment(field.offer.endsAt).format("MMM D") + '</td>' +
                        '<td><a href="javascript:;" class="publishProduct">' + ((field.offer.active == false) ? 'Publish' : '') + '</a></td>' +
                        '</tr>';
                    $("#past_offer").append(row);
                });
            }
        }
    });
}

$(".datepicker-head").on("change", function dateChange() {

    var sDate = $(".startDate").val();
    var eDate = $(".endDate").val();
    if (moment(sDate) > moment(eDate)) {
        $(".startDate").val(localStorage.getItem("start_date"));
        $(".endDate").val(localStorage.getItem("end_date"));
        //alert("start date should be small then end date");
        swal({
            title: "",
            text: dateErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () {
                // $("#searchText").focus();
            });
    } else {
        localStorage.setItem("start_date", sDate);
        localStorage.setItem("end_date", eDate);
        loadData(sDate, eDate);
    }

});

$(document).on("click", ".publishProduct", function () {
    let offerId = $(this).parents('tr').data('offer-id');

    $.ajax({
        url: base_url + 'offers/' + offerId,
        type: "PATCH",
        dataType: "json",
        data: {
            "active": true
        },
        success: function (response) {
            swal({
                title: "Success",
                text: "Offer Published Successfully. You will be redirected to Offer Edit screen to validate offer.",
                type: "success",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            }, function () {
                // location.reload();
                window.location.href = 'edit_offer?id=' + offerId
            });
        }
    });
})

/* For ordering according to Revenue/Date */
$('input[name=selector]').on('change', function () {
    var filter = $('input[name=selector]:checked').val();

    var $divs = $("tr.offer");
    if (filter === 'Revenue') {
        //sort by revenue asceding
        var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
            return $(a).find(".productRevenue a").text() > $(b).find(".productRevenue a").text();
        });
        console.log(alphabeticallyOrderedDivs);
        $("#past_offer").html('');
        $("#past_offer").append(alphabeticallyOrderedDivs);
    } else {
        //sort product date
        var numericallyOrderedDivs = $divs.sort(function (a, b) {
            //return $(a).find(".offer .productDate").text() > $(b).find("td.offer td.productDate").text();
            return $(a).find(".productDate").html() > $(b).find(".productDate").html();
        });
        console.log(numericallyOrderedDivs);
        $("#past_offer").html('');
        $("#past_offer").append(numericallyOrderedDivs);
    };
});

function sortResultsByDate(people, prop, asc) {

    people = people.sort(function (a, b) {
        var a = a.offer.updatedAt;
        var b = b.offer.updatedAt;

        if (a > b) {            // a comes first
            return -1
        } else if (b > a) {     // b comes first
            return 1
        } else {                // equal, so order is irrelevant
            return 0            // note: sort is not necessarily stable in JS
        }
    });
    console.log(people);
    return people;

}