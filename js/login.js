/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global base_url */

/*********************************Login***********************************/
var userid = localStorage.getItem('userId');
var establishmentId = localStorage.getItem('establishmentId');
var isAdminPaymentSetup = false;
if (userid && establishmentId) {
    window.location = 'orders';
}

var push_token = '';
$(document).ready(function () {
    localStorage.setItem("deviceId", 'NULL');
    // messaging.requestPermission()
    //     .then(function () {
    //         console.log('Notification permission granted.');
    //         return messaging.getToken();
    //     })
    //     .then(function (token) {
    //         push_token = token;
    //         localStorage.setItem("deviceId", token);
    //         console.log(token); // Display user token
    //     })
    //     .catch(function (err) { // Happen if user deny permission
    //         if (err.code == "messaging/permission-default") {
    //             console.log('Notification: The required permissions were not granted and dismissed instead.');
    //             swal({
    //                 title: "Notifications Permission Denied",
    //                 text: "The required permissions were not granted and dismissed instead. You will NOT receive any order Updates automatically.",
    //                 type: "error",
    //                 showCancelButton: false,
    //                 confirmButtonClass: "btn-danger",
    //                 confirmButtonText: "Ok",
    //                 closeOnConfirm: true
    //             });
    //         } else if (err.code == "messaging/permission-blocked") {
    //             console.log('Notification: The required permissions were not granted and blocked instead.');
    //             swal({
    //                 title: "Notifications Permission Blocked",
    //                 text: "The required permissions were not granted and dismissed instead. You will NOT receive any order Updates automatically.",
    //                 type: "error",
    //                 showCancelButton: false,
    //                 confirmButtonClass: "btn-danger",
    //                 confirmButtonText: "Ok",
    //                 closeOnConfirm: true
    //             });
    //         }
    //     });
});

function login(event) {

    event.preventDefault(); //prevent page refresh
    var email = $("#email").val();
    var password = $("#password").val();
    console.log(base_url);
    $.ajax({
        type: 'GET',
        url: base_url + 'bevviutils/bizAcctLogin?emailId=' + email + '&password=' + encodeURIComponent(password),
        datatype: "application/json",
        success: function (data) {
            access_token = data[0].id;
            if (typeof (Storage) !== "undefined") {
                localStorage.setItem('userId', data[0].userId);
                localStorage.setItem('usertoken', data[0].id);
                getUserDetails(data[0].userId);
            } else {
                alert('Local Storage not Available.');
            }
        },
        error: function (data) {
            if (data.responseJSON.error) {
                swal({
                    title: "",
                    text: data.responseJSON.error.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                        $("#email").focus();
                    });
            }
        }
    });
}

function getUserDetails(userid) {
    url = base_url + 'bizaccounts/' + userid + '?filter={"include":"paymentIds"}&access_token=' + access_token;
    $.getJSON(url, function (data) {

        if (data.paymentIds.length > 0) {
            isAdminPaymentSetup = true;
        }

        if (typeof data.establishmentIds != 'undefined') {
            localStorage.setItem('establishmentId', data.establishmentIds[0]);
        } else {
            localStorage.setItem('establishmentId', data.establishmentId[0]);
        }
        localStorage.setItem('email', data.email);
        if (data.firstName != "string") {
            localStorage.setItem('user-name', data.firstName);
        } else {
            localStorage.setItem('user-name', 'Admin');
        }

        $.getJSON(base_url + '/establishments/' + localStorage.getItem('establishmentId') + '?filter={"include":"photos"}', function (edata) {
            localStorage.setItem('shopname', edata.name);

            if (edata.photos.length > 0) {
                path = edata.photos[0].file.path;
                container = path.split('/')[1];
                filename = path.split('/')[2];
                image = base_url + 'storages/' + container + '/download/' + filename;
                localStorage.setItem('profilePicImage', image);
                $("#profilePicImage").attr("src", image);
            }
            else {
                //If no image found then set default image
                $("#profilePicImage").attr("src", 'https://dummyimage.com/78x78/9d2481/ffffff');
            }
            console.log(edata);

            ///Delete all existing devices of user
            $.ajax({
                url: base_url + 'bizaccounts/' + localStorage.getItem('userId') + '/devices?access_token=' + localStorage.getItem('usertoken'),
                type: 'delete',
                complete: function (params) {
                    console.log(params);
                    if (params.status == 204) {
                        //Add last login device detail
                        if (localStorage.getItem("deviceId") != 'NULL') {
                            $.ajax({
                                url: base_url + 'devices',
                                type: 'post',
                                data: {
                                    "uuid": localStorage.getItem("deviceId"), //Device Id stored using firebase
                                    "bizAccountId": localStorage.getItem('userId'),
                                },
                                success: function (param) {
                                    console.log(param);
                                    if (isAdminPaymentSetup) {
                                        window.location = 'orders';
                                    } else {
                                        window.location = stripeMerchantOnboardingUrl;
                                    }
                                }
                            });
                        } else {
                            if (isAdminPaymentSetup) {
                                window.location = 'orders';
                            } else {
                                window.location = stripeMerchantOnboardingUrl;
                            }
                        }
                    } else {
                        swal({
                            title: "",
                            text: "Request Not completed",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                if (isAdminPaymentSetup) {
                                    window.location = 'orders';
                                } else {
                                    window.location = stripeMerchantOnboardingUrl;
                                }
                            });
                    }
                },
                error: function (param) {
                    console.log(param);
                }
            });

        });
    });
}