$(document).ready(function () {
    loadOrderDetail(params);
});

function loadOrderDetail(orderNumber) {
    $.ajax({
        url: base_url + 'orders?filter={"include":["account",{"orderdetails":["product"]}], "where":{"orderNumber":"' + params + '"}}',
        success: function (data) {
            if (data.length > 0) {
                data = data[0];
                $(".order-num").html('Order Number ' + data.orderNumber + ' is being delivered to:');
                $(".username").html(data.account.username);
                $(".address").html(data.address);
                $(".phone").html(data.account.phoneNumber);
                $(".email").html(data.account.email);
                // $(".order-num").html('Order Number ' + data.orderNumber);
                var subtotal = 0;
                // var rewards = 0;
                // var rewards = 0;
                $.each(data.orderdetails, function (i, order) {
                    if (data.status == 0) {
                        status = 'Pending'
                    } else if (data.status == 1) {
                        status = 'Fulfilled';
                    } else if (data.status == 2) {
                        status = 'Delivered';
                    }

                    var item = '<tr>' +
                        '<td class="order-items">' + order.product.name +
                        '<br>' +
                        '<span>' + order.product.category + '</span>' +
                        '</td>' +
                        '<td>' + order.product.upc + '</td>' +
                        '<td>' + order.quantity + '</td>' +
                        '<td>$' + (order.price * order.quantity).toFixed(2) +
                        '<br>' +
                        '<span class="line-through hidden">$' + order.actualPrice.toFixed(2) + '</span>' +
                        '</td>' +
                        '<td>' + moment(data.deliveryTime).format('MMM D. dddd') + //Nov 1. Friday
                        '<br> ' + moment(data.deliveryTime).format('ha') + '</td>' +
                        '<td>' + status + '</td>' +
                        '</tr>';

                    //rewards = rewards + ((order.actualPrice - order.price) * order.quantity); //    (original price - sale price) * qty
                    subtotal = subtotal + parseFloat(order.price * order.quantity);
                    $(".order-details-table tbody").append(item);
                });
                if (data.tax != null) {
                    tax = data.tax.toFixed(2);
                } else {
                    tax = '0.0';
                }
                $(".subtotal").html('$' + subtotal.toFixed(2));
                $(".tax").html('$' + tax);
                $(".bevviDisc").html('$(' + parseFloat(data.discountApplied.promoDisc).toFixed(2) + ')');
                $(".storeDisc").html('$(' + parseFloat(data.discountApplied.volumeDisc).toFixed(2) + ')');
                $(".paid").html('$' + data.totalAmount.toFixed(2));
            } else {
                //alert("No Data Found !");
                $(".card-body").html('<h3 style="text-align:center">No Data Found !</h3>');
            }
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("userId");
                        window.location = 'login';
                    });
            }
        }
    });
}

$(".search-btn").click(function () {
    search = $(".transaction-search").val().trim();
    if (search == '') {
        //alert('Please Enter Order Number');
        swal("", "Please Enter Order Number!", "error");
        $(".confirm-code input").focus();
        //loadTransactions(dataset);
    } else {
        window.location = "transaction-details?id=" + search;
        // result = $.grep(dataset, function (e) { return e.orderNum == search });
        // if (result.length > 0) {
        //     loadTransactions(result);
        // }
        // else {
        //     swal("", "No Data found !","error");         
        //     $(".transaction-search").focus();
        // }
    }
});

function printDiv() {
    window.open('print-receipt-final?id=' + params, '_blank');

    // $("aside").hide(); //Sidebar
    // $(".breadcrumb").hide(); //Breadcrumb
    // $(".btn-print").hide(); //Print button
    // $(".searchbar").hide(); //Print button
    // $(".card").show(); //Show area of print

    // window.print();
    // $("aside").show();
    // $(".searchbar").show(); //Print button
    // $(".breadcrumb").show();
    // $(".btn-print").show();
}