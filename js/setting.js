/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global access_token, base_url, errorMessage, moment */

var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}

/*store hours*/
var days = {
    2: "Monday",
    3: "Tuesday",
    4: "Wednesday",
    5: "Thursday",
    6: "Friday",
    7: "Saturday",
    1: "Sunday"
};
var radius = 0;
function changeButtonClick() {
    $('#changePasswordModel').modal('show');
}

$("#submitBtn").click(function () {
    var currentPass = $("#currentPass").val();
    var confirmPass1 = $("#confirmPass1").val();
    var confirmPass2 = $("#confirmPass2").val();
    if (currentPass.trim() === '') {
        swal({
            title: "",
            text: "Current Password can not be blank !",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () { });
    }
    if (confirmPass1 === confirmPass2) {
        $.ajax({
            url: base_url + "bevviutils/changeBizPassword?emailId=" + localStorage.getItem('email') + "&curpassword=" + encodeURIComponent(currentPass) + "&newpassword=" + encodeURIComponent(confirmPass1),
            success: function (response) {
                swal({
                    title: "",
                    text: response.message,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                }, function () {
                    $('#changePasswordModel').modal('hide');
                });
            },
            error: function (response) {
                swal({
                    title: "",
                    text: response.responseJSON.error.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                });
            }
        });
    } else {
        swal({
            title: "",
            text: "New Password and Confirm Password are not Same !",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () { });
    }
});

$(document).ready(function () {
    $("#isBeyondFreeRadiusActive").change(function () {
        if ($(this).is(':checked')) {
            $('.minOutsideOrderVal').attr("disabled", false);
            $('.outsideDeliveryCharge').attr("disabled", false);
        } else {
            $('.minOutsideOrderVal').val('').attr("disabled", true);
            $('.outsideDeliveryCharge').val('').attr("disabled", true);
        }
    });

    $(".isInsideChargePerBtl").change(function () {
        if (!$(this).is(":checked")) {
            $(".insideFlatFee").attr("disabled", false);
            $(".insideDeliveryCharge").attr("disabled", true);
        } else {
            $(".insideFlatFee").attr("disabled", true);
            $(".insideDeliveryCharge").attr("disabled", false);
        }
    });

    $(".isOutsideChargePerBtl").change(function () {
        if (!$(this).is(":checked")) {
            $(".outsideFlatFee").attr("disabled", false);
            $(".outsideDeliveryCharge").attr("disabled", true);
        } else {
            $(".outsideFlatFee").attr("disabled", true);
            $(".outsideDeliveryCharge").attr("disabled", false);
        }
    });

    $(".isShippingChargePerBtl").change(function () {
        if (!$(this).is(":checked")) {
            $(".shippingFlatFee").attr("disabled", false);
            $(".shippingChargePerBtl").attr("disabled", true);
        } else {
            $(".shippingFlatFee").attr("disabled", true);
            $(".shippingChargePerBtl").attr("disabled", false);
        }
    });

    $(".accountNumber").attr("disabled", "disabled");
    $.ajax({
        url: base_url + "bizaccounts/" + localStorage.getItem('userId') + "?access_token=" + access_token,
        success: function (response) {
            //added on 12-04-2018 to manage establishment Id and Ids both.
            if (typeof response.establishmentIds !== 'undefined') {
                var establishmentId = response.establishmentIds[0];
            } else {
                var establishmentId = response.establishmentId[0];
            }

            /* Set Admin Information*/
            $(".adminName").val(response.firstName);
            $(".accountNumber").val(response.accountNum);
            $(".eMail").val(response.email);
            $(".admin_address").val(response.address);
            $(".website").val(response.website);
            $(".phoneNumber").val(response.phoneNum);

            $.get({
                url: base_url + 'establishments/' + establishmentId,
                success: function (data) {
                    if (typeof data.deliveryZipCodes != "undefined") {
                        $(".gi_delivery_zip_codes").val(data.deliveryZipCodes.join(', '));
                    }

                    $(".gi_Name").val(data.name);
                    $(".gi_Address").val(data.address);
                    $(".gi_Phone").val(data.phoneNum);
                    $(".gi_Email").val(data.email);
                    $(".gi_additional_Email").val(data.ccList);
                    $(".gi_Website").val(data.url);

                    //Set them disabled
                    $(".gi_Name").attr("disabled", "disabled");
                    $(".gi_Address").attr("disabled", "disabled");
                    $(".gi_Phone").attr("disabled", "disabled");
                    $(".gi_Email").attr("disabled", "disabled");
                    $(".gi_Website").attr("disabled", "disabled");

                    $.each(data.operatesAt.timeframes, function (i, time) {
                        var start_time = time.open[0].start;
                        var end_time = time.open[0].end;

                        $.each(time.days, function (k, day) {
                            switch (parseInt(day)) {
                                case 2: //moday
                                    $("#mon .start").val(moment(start_time, 'h:mm a').format('h:mm A')); // 10:00 AM
                                    $("#mon .end").val(moment(end_time, 'h:mm a').format('h:mm A')); // 8:00 PM
                                    break;
                                case 3:
                                    $("#tue .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                                    $("#tue .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                                    break;
                                case 4:
                                    $("#wed .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                                    $("#wed .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                                    break;
                                case 5:
                                    $("#thur .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                                    $("#thur .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                                    break;
                                case 6:
                                    $("#fri .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                                    $("#fri .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                                    break;
                                case 7: //saturday
                                    $("#sat .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                                    $("#sat .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                                    break;
                                case 1: //sunday
                                    $("#sun .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                                    $("#sun .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                                    break;
                            }
                        });
                    });

                    //Set Discount Details
                    $(".minQty").val(data.minQty);
                    $(".percentDiscount").val(data.percentDiscount);
                }
            });
        },
        error: function (response) {
            console.log(response);
            if (response.status === 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("userId");
                        window.location = 'login';
                    });
            }
        }
    });
    $.ajax({
        url: base_url + 'deliverycharges?filter={"where":{"bizAccountId":"' + localStorage.getItem('userId') + '", "establishmentId":"' + localStorage.getItem('establishmentId') + '"}}&access_token=' + access_token,
        success: function (response) {
            if (response[0] != undefined) {
                $('.minInsideOrderVal').val(response[0].insideMinVal);
                $('.insideDeliveryCharge').val(response[0].insideChargePerBtl);
                $(".minDeliveryTime").val(response[0].minDeliveryTime);

                $(".isInsideChargePerBtl").attr("checked", response[0].isInsideChargePerBtl).trigger("change");
                $(".isOutsideChargePerBtl").attr("checked", response[0].isOutsideChargePerBtl).trigger("change");
                $(".insideFlatFee").val(response[0].insideFlatFee);
                $(".outsideFlatFee").val(response[0].outsideFlatFee);

                if (response[0].deliverOutside == true) {
                    $("#isBeyondFreeRadiusActive").attr("checked", true).trigger("change");
                    $('.minOutsideOrderVal').val(response[0].outsideMinVal).attr("disabled", false);
                    $('.outsideDeliveryCharge').val(response[0].outsideChargePerBtl).attr("disabled", false);
                } else {
                    $("#isBeyondFreeRadiusActive").attr("checked", false).trigger("change");
                    $('.minOutsideOrderVal').val(response[0].outsideMinVal).attr("disabled", true);
                    $('.outsideDeliveryCharge').val(response[0].outsideChargePerBtl).attr("disabled", true);
                }
                radius = response[0].freeRadius;
                $("#range").ionRangeSlider({
                    hide_min_max: true,
                    keyboard: true,
                    min: 0,
                    max: 10,
                    from: response[0].freeRadius,
                    step: 0.1,
                    postfix: " mile",
                    onChange: function (data) {
                        console.log("change", data);
                        radius = data.from;
                    },
                    onFinish: function (data) {
                        console.log("finish", data);
                        radius = data.from;
                    },
                    onUpdate: function (data) {
                        console.log("update", data);
                        radius = data.from;
                    }
                });
            } else {
                $("#range").ionRangeSlider({
                    hide_min_max: true,
                    keyboard: true,
                    min: 0,
                    max: 10,
                    from: 1,
                    step: 0.1,
                    postfix: " mile",
                    onChange: function (data) {
                        console.log("change", data);
                        radius = data.from;
                    },
                    onFinish: function (data) {
                        console.log("finish", data);
                        radius = data.from;
                    },
                    onUpdate: function (data) {
                        console.log("update", data);
                        radius = data.from;
                    }
                });
            }
        }
    });

    // Shipping
    $.ajax({
        url: base_url + 'shippingcharges?filter={"where":{"establishmentId":"' + localStorage.getItem('establishmentId') + '"}}&access_token=' + access_token,
        success: function (response) {
            console.log(response[0])
            if (response[0] != undefined) {
                $('.minShippingOrderVal').val(response[0].minAmtForFreeShipping);
                $('.shippingChargePerBtl').val(response[0].chargePerBtl);
                if (response[0].chargePerBtl > 0) {
                    $(".isShippingChargePerBtl").attr("checked", true).trigger("change");
                } else {
                    $(".isShippingChargePerBtl").attr("checked", false).trigger("change");
                }
                
            }
        }
    });
});

function timeConvert(time) {
    if (time === '' || time.trim() === '') {
        time = "00:00 AM";
    }
    return moment(time, 'h:m A').format('HHmm');
}

$("#saveBtn").click(function () {
    var email = $(".eMail").val();
    var additional_Email = $(".gi_additional_Email").val();
    var delivery_zip_codes = $(".gi_delivery_zip_codes").val();
    var deliveryZipCodeToSave = [];
    if (delivery_zip_codes != "") {
        $.each($(".gi_delivery_zip_codes").val().split(','), function (index, item) {
            deliveryZipCodeToSave.push(item.trim())
        })
    }
    var accountNumber = $(".accountNumber").val();
    var adminName = $(".adminName").val();
    var phoneNumber = $(".phoneNumber").val();
    var admin_address = $(".admin_address").val();
    var admin_website = $(".website").val();


    var minInside = $(".minInsideOrderVal").val();
    var insideBottleCharge = $(".insideDeliveryCharge").val();
    var minOutside = $(".minOutsideOrderVal").val();
    var outsideBottleCharge = $(".outsideDeliveryCharge").val();
    var insideFlatFee = $(".insideFlatFee").val();
    var outsideFlatFee = $(".outsideFlatFee").val();
    var minDeliveryTime = $(".minDeliveryTime").val();

    var minShippingOrderVal = $(".minShippingOrderVal").val();
    var shippingChargePerBtl = $(".shippingChargePerBtl").val();
    var shippingFlatFee = $(".shippingFlatFee").val();

    var minQty = $(".minQty").val();
    var percentDiscount = $(".percentDiscount").val();

    console.log("radius," + radius);
    console.log("minInside," + minInside);
    console.log("insideBottle," + insideBottleCharge);
    console.log("minOut, " + minOutside);
    console.log("outsideBottle," + outsideBottleCharge);


    var mon = [timeConvert($('#mon .start').val()), timeConvert($('#mon .end').val())];
    var tue = [timeConvert($('#tue .start').val()), timeConvert($('#tue .end').val())];
    var wed = [timeConvert($('#wed .start').val()), timeConvert($('#wed .end').val())];
    var thur = [timeConvert($('#thur .start').val()), timeConvert($('#thur .end').val())];
    var fri = [timeConvert($('#fri .start').val()), timeConvert($('#fri .end').val())];
    var sat = [timeConvert($('#sat .start').val()), timeConvert($('#sat .end').val())];
    var sun = [timeConvert($('#sun .start').val()), timeConvert($('#sun .end').val())];

    //In mobile week sunday to saturday
    var schedule = {
        "timeframes": [{
            "days": [
                2
            ],
            "open": [{
                "start": mon[0],
                "end": mon[1]
            }]
        },
        {
            "days": [
                3
            ],
            "open": [{
                "start": tue[0],
                "end": tue[1]
            }]
        },
        {
            "days": [
                4
            ],
            "open": [{
                "start": wed[0],
                "end": wed[1]
            }]
        },
        {
            "days": [
                5
            ],
            "open": [{
                "start": thur[0],
                "end": thur[1]
            }]
        },
        {
            "days": [
                6
            ],
            "open": [{
                "start": fri[0],
                "end": fri[1]
            }]
        },
        {//Saturday
            "days": [
                7
            ],
            "open": [{
                "start": sat[0],
                "end": sat[1]
            }]
        },
        {//Sunday
            "days": [
                1
            ],
            "open": [{
                "start": sun[0],
                "end": sun[1]
            }]
        }
        ]
    };

    swal({
        title: "",
        text: "Save Admin Info ?",
        //type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Save",
        closeOnConfirm: true
    },
        function () {
            // //Save BizAccount details
            $.ajax({
                url: base_url + 'bizaccounts/' + localStorage.getItem('userId') + '?access_token=' + access_token,
                data: {
                    firstName: adminName,
                    email: email,
                    accountNum: accountNumber,
                    phoneNum: phoneNumber,
                    address: admin_address,
                    website: admin_website,
                    establishmentIds: [localStorage.getItem('establishmentId')]
                },
                type: "PATCH",
                dataType: "json",
                success: function (data) {
                    // swal({
                    //     title: "",
                    //     text: "Account Updated Successfully.",
                    //     type: "success",
                    //     showCancelButton: false,
                    //     confirmButtonClass: "btn-primary",
                    //     confirmButtonText: "Ok",
                    //     closeOnConfirm: true
                    // },
                    // function () {
                    // });
                }
            });

            let establishmentDataToUpdate = {
                //phoneNum: phoneNumber,
                operatesAt: schedule,
                bizAccountId: localStorage.getItem('userId'),
                ccList: additional_Email,
                minQty: minQty,
                deliveryZipCodes: deliveryZipCodeToSave,
                percentDiscount: percentDiscount
            };
            // // //Save Establishment details
            $.ajax({
                url: base_url + 'establishments/' + localStorage.getItem('establishmentId'),
                data: JSON.stringify(establishmentDataToUpdate),
                contentType: "application/json",
                type: "PATCH",
                dataType: "json",
                success: function (response) {
                    // console.log(response);
                    // //alert("Establishment Updated Successfully.");
                    // swal({
                    //     title: "",
                    //     text: "Account Details Updated Successfully.",
                    //     type: "success",
                    //     showCancelButton: false,
                    //     confirmButtonClass: "btn-primary",
                    //     confirmButtonText: "Ok",
                    //     closeOnConfirm: true
                    // },
                    //     function () {
                    //         window.location.reload();
                    //     });

                }
            });


            //delivery save
            $.ajax({
                url: base_url + 'deliverycharges/upsertWithWhere?where={"bizAccountId":"' + localStorage.getItem('userId') + '", "establishmentId":"' + localStorage.getItem('establishmentId') + '"}',
                data: {
                    bizAccountId: localStorage.getItem('userId'),
                    establishmentId: localStorage.getItem('establishmentId'),
                    freeRadius: radius,
                    insideMinVal: minInside,
                    isInsideChargePerBtl: $(".isInsideChargePerBtl").is(":checked"),
                    insideChargePerBtl: insideBottleCharge,
                    insideFlatFee: insideFlatFee,
                    deliverOutside: $("#isBeyondFreeRadiusActive").is(":checked"),
                    outsideMinVal: minOutside,
                    isOutsideChargePerBtl: $(".isOutsideChargePerBtl").is(":checked"),
                    outsideChargePerBtl: outsideBottleCharge,
                    outsideFlatFee: outsideFlatFee,
                    minDeliveryTime: minDeliveryTime
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    //alert("Establishment Updated Successfully.");
                    // swal({
                    //     title: "",
                    //     text: "Account Details Updated Successfully.",
                    //     type: "success",
                    //     showCancelButton: false,
                    //     confirmButtonClass: "btn-primary",
                    //     confirmButtonText: "Ok",
                    //     closeOnConfirm: true,
                    //     closeOnClickOutside: false
                    // },
                    //     function () {
                    //         window.location.reload();
                    //     });

                }
            });

            //Shipping save
            $.ajax({
                url: base_url + 'shippingcharges/upsertWithWhere?where={"establishmentId":"' + localStorage.getItem('establishmentId') + '"}',
                data: {
                    minAmtForFreeShipping: minShippingOrderVal,
                    flatFee: shippingFlatFee,
                    chargePerBtl: shippingChargePerBtl
                },
                type: "POST",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    //alert("Establishment Updated Successfully.");
                    swal({
                        title: "",
                        text: "Account Details Updated Successfully.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-primary",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true,
                        closeOnClickOutside: false
                    },
                        function () {
                            window.location.reload();
                        });

                }
            });
        });
});