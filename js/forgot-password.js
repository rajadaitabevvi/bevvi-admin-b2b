$("#resetBtn").click(function () {
    var email = $("#email").val();
    if (email == '') {
        //alert('Please Enter Email !');
        swal({
            title: "",
            text: 'Please Enter Email !',
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () {
                $("#email").val('');
            });
    }
    else {
        var validEmail = validateEmail(email);
        if (validEmail) {
            $.ajax({
                url: base_url + 'bevviemails/{id}/sendEmail?type=2&emailId=' + email,
                success: function (data) {

                    if (data.error) {
                        swal({
                            title: "",
                            text: data.error.message,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                $("#email").focus();
                            });
                    }
                    else {
                        swal({
                            title: "",
                            text: data.message,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                window.location = 'login';
                            });
                    }

                }
            });
        }
        else {
            //alert('Invalid Email !');
            swal({
                title: "",
                text: 'Invalid Email !',
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
                function () {
                    $("#email").val('');
                });
        }
    }
});

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}