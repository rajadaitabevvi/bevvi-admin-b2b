/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//include header file
var isAdminPaymentSetup = false;
$("#header").load("includes/header.html", function () {
    loadDataIntoHeader();
});
//set base url

/*  GET PARAMETER FROM URL */
var url = window.location.href;
console.log(url);
// var curpage;
// var new_Ar;
// var params;
// if (url.split('/')[4] != undefined) {
    var curpag = url.split('/')[3]; // Development
    // var curpag = url.split('/')[4]; // Production
    var new_Ar = curpag.split('?');
    // curpag = url.split('/')[4];
    // new_Ar = curpag.split('?');
    if (new_Ar.length > 1) {
        // var params = new_Ar[1].split('=')[1];
        var params = new_Ar[1].split('=')[1];
        console.log(params);
    }
// }



/*  GET PARAMETER FROM URL */
if (url.split('/')[2] === '192.168.10.67' || url.split('/')[2] === 'localhost' || url.split('/')[2] === 'localhost:8888' || url.split('/')[2] === "dev.business.getbevvi.com"
|| url.split('/')[2] === "dev.getbevvi.com") {
    var base_url = 'http://dev.getbevvi.com:4000/api/';
    // var base_url = 'http://localhost:4000/api/';
    var order_url = 'http://dev.getbevvi.com:3038/capture_charge_b2b';
    var corp_order_url = 'http://dev.getbevvi.com:3038/capture_corp_charge_b2b';
    var img_url = 'http://dev.getbevvi.com:3038/upload_product_image_b2b';
    var img_url1 = 'http://dev.getbevvi.com:3038/upload_establishment_image_b2b';
    // var stripeMerchantOnboardingUrl = 'orders';
    var stripeMerchantOnboardingUrl = 'https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_C4OyT3meaW8oY8TN77kTvJOyWmaIWdVl&scope=read_write&redirect_uri=http://dev.getbevvi.com:3038/stripeOnboarding_b2b';
} else {
    var base_url = 'https://getbevvi.com:4000/api/';
    var order_url = 'https://getbevvi.com:3038/capture_charge_b2b';
    var corp_order_url = 'https://getbevvi.com:3038/capture_corp_charge_b2b';
    var img_url = 'https://getbevvi.com:3038/upload_product_image_b2b';
    var img_url1 = 'https://getbevvi.com:3038/upload_establishment_image_b2b';
    var stripeMerchantOnboardingUrl = 'https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_C4OySqgzI0yuX6IqksG5QyDtHALuIUTU&scope=read_write&redirect_uri=https://getbevvi.com:3038/stripeOnboarding_b2b';
}

// For Shradesh Testing Purpose
// TODO: To remove the line
// Start
// var base_url = 'http://localhost:4000/api/';
// var order_url = 'http://localhost:3038/capture_charge_b2b';
// var img_url = 'http://localhost:3038/upload_product_image_b2b';
// var img_url1 = 'http://localhost:3038/upload_establishment_image_b2b';
// var stripeMerchantOnboardingUrl = 'https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_C4OyT3meaW8oY8TN77kTvJOyWmaIWdVl&scope=read_write&redirect_uri=http://localhost:3038/stripeOnboarding_b2b';
// var stripeMerchantOnboardingUrl = 'orders';
// END

var errorMessage = "You have been logged out. Please log back in."; // For error if token is changes
var access_token = localStorage.getItem('usertoken');
var userId = localStorage.getItem('userId');
var dateErrorMessage = "Start date should be smaller than end date";
var deleteConfirmMessage = "Are you sure to delete this ?";


function clearLocalStorage() {
    localStorage.removeItem("userId");
    localStorage.removeItem("establishmentId");
    localStorage.removeItem("deviceId");
    localStorage.removeItem("email");
    localStorage.removeItem("end_date");
    localStorage.removeItem("favorite_tables");
    localStorage.removeItem("offers");
    localStorage.removeItem("shopname");
    localStorage.removeItem("start_date");
    localStorage.removeItem("user-name");
    localStorage.removeItem("usertoken");
}
/*****************************Logout***********************************************/
$(document).on('click', '.signout', function () {
    /*clear localStorage variable on click of logout. */
    // localStorage.removeItem('userId');
    // localStorage.removeItem('user-name');
    // localStorage.removeItem('shopname');
    // localStorage.removeItem('offers');
    // localStorage.removeItem('establishmentId');
    // localStorage.removeItem('usertoken');
    // localStorage.clear();

    $.post({
        url: base_url + 'bizaccounts/logout?access_token=' + access_token,
        success: function () { },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        clearLocalStorage();
                        window.location = 'login';
                    });
            }
        },
        complete: function (xhr) {
            if (xhr.status === 204) {
                clearLocalStorage();
                window.location = 'login';
            }
        }
    });
});
/*****************************Logout***********************************************/

/*  IMAGE UPLOAD for Establishment */
$(".photo span").html('');
$(document).on('click', '#profileImageBtn', function () {

    //    alert(1);
    //Upload Image to server
    var image = $('#profilePic').get(0).files[0];
    console.log(image);
    var formData = new FormData();
    formData.append("image", image);
    formData.append("establishmentId", localStorage.getItem("establishmentId"));
    $.ajax({
        url: img_url1,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (status) {
            console.log(status);
            path = status.path;
            container = path.split('/')[1];
            filename = path.split('/')[2];
            new_image = base_url + 'storages/' + container + '/download/' + filename;
            localStorage.setItem('profilePicImage', new_image);
            $("#profilePicImage").attr("src", new_image);
            $(".photo span").html("Image uploaded Successfully.");
            setTimeout(function () {
                location.reload();
            }, 2000);
        },
        error: function (error) {
            console.log(error);
        }
    });
});

/*  IMAGE UPLOAD for Establishment */

function loadDataIntoHeader() {
    var url = window.location.href;
    var curpag = url.split('/')[3]; //Development
    // var curpag = url.split('/')[4]; //Production
    var new_Ar = curpag.split('?');
    var curpage = new_Ar[0];
    if (new_Ar.length > 1) {
        var params = new_Ar[1].split('=')[1];
    }
    if (curpage === 'establishment' || curpage === 'account') {
        $('aside').addClass('hide');
    }
    if (curpage === 'past-offers' || curpage === 'add-new-offer' || curpage === 'edit-offer' || curpage === 'current-offer') {
        curpage = 'offers';
    } else if (curpage === 'transaction-details') {
        curpage = 'transactions';
    } else if (curpage === 'print-receipt') {
        curpage = 'orders';
    }

    $(".side-menu a").each(function () {
        var page = $(this).attr('href');
        if (curpage === page) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
    var username = localStorage.getItem('user-name');
    var shopname = localStorage.getItem('shopname');

    var profilePicImage = localStorage.getItem('profilePicImage');
    if (profilePicImage != null) {
        $("#profilePicImage").attr("src", profilePicImage);
    }

    //$("#profilePicImage").attr("src", image);
    if (username != '' || username != null) {
        $('.dropdown-toggle span').text(username);
    }
    if (shopname != '' || shopname != null) {
        $('.navbar-brand').text(shopname);
    }
}

/*****************************Page Active indicator***********************************************/
$(document).ready(function () {
    loadDataIntoHeader();
    if (url.split('/')[2] === '192.168.10.67' || url.split('/')[2] === 'localhost' || url.split('/')[2] === 'localhost:8888' || url.split('/')[2] === "dev.busniess.getbevvi.com") {

    } else {
        if (userId != null && access_token != null) {
            checkStripeAccountLink(userid, access_token);
        }
    }
});


//Restrict right click and console 
/*
document.onkeydown = function (e) {
    if (event.keyCode == 123) {
        return false;
    }
    if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
        return false;
    }
    if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
        return false;
    }
    if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
        return false;
    }
    if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
        return false;
    }
}
$(document).ready(function () {
    $(document).bind("contextmenu", function (e) {
        return false;
    });

});
*/
/***************************Push Notification*************************************************/


// Initialize Firebase
var config = {
    apiKey: "AIzaSyAtqhpavJIdPUdx_FEky7mXaOOD78vQrHU",
    authDomain: "bevvi-495d6.firebaseapp.com",
    databaseURL: "https://bevvi-495d6.firebaseio.com",
    projectId: "bevvi-495d6",
    storageBucket: "bevvi-495d6.appspot.com",
    messagingSenderId: "263058269693"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.onMessage(function (payload) {
    console.log("payload :: ", payload);
    var instance = JSON.parse(payload.data.instance);
    // console.log("payload :: ", );
    // notification = [];
    // notification.push(payload);
    // localStorage.setItem('notification', notification);
    // var notificationdata = localStorage.getItem('notification');
    // $.each(notificationdata, function (i, noti) {
    //     console.log(noti);
    if (payload.data.type === "ORDER_CREATED") {
        $(document).on('click', '.order-alert-btn', function () {
            $('section.order-alert1').addClass('hide');
            location.reload();
        });
        $('section.order-alert1 .order_number span').text(instance.orderNum);
        $('section.order-alert1 h3.timing').text(moment(instance.deliveryTime).format('MMM DD, hh:mm A'));
        $('section.order-alert1').removeClass('hide');
    } else if (payload.data.type === "Scheduled Order Placed") {
        $(document).on('click', '.order-alert-btn2', function () {
            $('section.order-alert2').addClass('hide');
        });
        $('section.order-alert2 .order_number span').text(payload.data.orderid);
        $('section.order-alert2 h3.timing').text("Delivery at " + moment(payload.data.time, 'DD-MM-YYYY, hh:mm A').format('hh:mm A'));
        $('section.order-alert2').removeClass('hide');
    } else if (payload.data.type === "ORDER_CANCELLED") {
        $(document).on('click', '.cancel-alert-btn', function () {
            $('section.cancel-alert1').addClass('hide');
        });
        $('section.cancel-alert1 .order_number span').text(payload.data.orderid);
        $('section.cancel-alert1').removeClass('hide');
    }
    //    });



    //    console.log('onMessage',body_data);

});

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    return false;
}

function checkStripeAccountLink(userid, access_key) {
    url = base_url + 'bizaccounts/' + userid + '?filter={"include":"paymentIds"}&access_token=' + access_token;
    $.getJSON(url, function (data) {
        if (data.paymentIds.length > 0) {
            isAdminPaymentSetup = true;
        }
        if (!isAdminPaymentSetup) {
            window.location = stripeMerchantOnboardingUrl;
        }
    });
}