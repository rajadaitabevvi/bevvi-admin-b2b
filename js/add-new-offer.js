/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var userid = localStorage.getItem('userId');
var checkInputInForm = false;
var croppedImageDataURL;
var croppedImageDataURLToUpload;
if (!userid) {
    window.location = 'login';
}
var productSelect;
var skuSelect;
var showWarning = true;
window.onbeforeunload = function () {
    if (showWarning) {
        return "Do you really want to leave our brilliant application?";
    } else {
        return;
    }
};
$(function () {
    disableAllFields();

    skuSelect = $('.js-data-sku-ajax').select2({
        tags: true,
        allowClear: true,
        minimumInputLength: 1,
        placeholder: "",
        selectOnBlur: true,
        ajax: {
            url: base_url + 'products',
            dataType: 'json',
            data: function (params) {
                var query = {
                    filter: '{"where":{"establishmentId":"' + localStorage.getItem('establishmentId') + '", "sku": { "like": "^' + params.term + '.*", "options": "i" }}, "include":["productPhoto"], "order":"sku asc"}'
                }
                return query;
            },
            processResults: function (data) {
                data.forEach(function (item) {
                    item.text = item.sku;
                    item.id = item.sku;
                });

                return {
                    results: data
                };
            }
        }
    }).on('select2:close', function () {
        enableAllFields();
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if (typeof data.name != 'undefined') {

            var newStateVal = data.name;
            if (typeof newStateVal != 'undefined') {
                if ($(".js-data-product-ajax").find('option[value="' + newStateVal + '"]').length) {
                    $(".js-data-product-ajax").val(newStateVal).trigger("change");
                } else {
                    var newState = new Option(newStateVal, newStateVal, true, true);
                    $(".js-data-product-ajax").append(newState).trigger('change');
                }
            }
            $('#upcCode').val(data.upc ? data.upc : '');
            $('#skuNumber').val(data.sku ? data.sku : '');
            $('#category').attr('disabled', false).val(data.category ? data.category : '').trigger("change");
            $("#subCategory option[value='" + data.subCategory + "']").attr("selected", true);
            $('#size').val(data.size ? data.size : '');
            $('#unit').val(data.units ? data.units : '');
            $('#alc_content').val(data.alcContent ? data.alcContent : '');
            $('#region').val(data.region ? data.region : '');
            $('#appellation').val(data.appellation ? data.appellation : '');
            $('.country_list').val(data.country ? data.country : '');
            $('#year').val(data.year ? data.year : '');
            $('#varietal').val(data.varietal ? data.varietal : '');
            $('#itemDescription').attr('disabled', false).val(data.description ? data.description : '');
            $('#color').val(data.color ? data.color : '');
            $('#body').val(data.body ? data.body : '');
            $('#originalPrice').val(data.price.toString() ? data.price : '');
            $('#discountPrice').val(data.price.toString() ? data.price : '');
            $('#aroma').val(data.aromaAndFlavor ? data.aromaAndFlavor : '');
            $('#foodPairings').val(data.pairings ? data.pairings : '');
            $("#isSponsored").attr("checked", data.isSponsored);
            $("#isVolumeDiscountEligible").attr("checked", data.isVolumeDiscountEligible);
            $("#isTaxable").attr("checked", data.isTaxable);
            $('#productImage').attr('src', data.productPhoto ? base_url + 'storages/bevvi/download/' + data.productPhoto.file.filename : 'images/no-img.png');
            $('#loader_img').show();
            if (!data.productPhoto) {
                $('#loader_img').hide();
            }
            $('#productImage').on('load', function () {
                $('#loader_img').hide();
            });
        }
    }).on('select2:unselect', function (e) {
        disableAllFields();
    });


    productSelect = $('.js-data-product-ajax').select2({
        tags: true,
        allowClear: true,
        minimumInputLength: 1,
        placeholder: "",
        selectOnBlur: true,
        ajax: {
            url: base_url + 'products',
            dataType: 'json',
            data: function (params) {
                var query = {
                    filter: '{"where":{"establishmentId":"' + localStorage.getItem('establishmentId') + '", "name": { "like": ".*' + params.term + '.*", "options": "i" }}, "include":["productPhoto"]}'
                }
                return query;
            },
            processResults: function (data) {
                data.forEach(function (item) {
                    item.text = item.name;
                    item.id = item.name;
                })
                return {
                    results: data
                };
            }
        }
    }).on('select2:close', function () {
        enableAllFields();
    }).on('select2:select', function (e) {
        var data = e.params.data;
        if (typeof data.name != 'undefined') {
            var newStateVal = data.sku;
            if (typeof newStateVal != 'undefined') {
                if ($(".js-data-sku-ajax").find("option[value='" + newStateVal + "']").length) {
                    $(".js-data-sku-ajax").val(newStateVal).trigger("change");
                } else {
                    var newState = new Option(newStateVal, newStateVal, true, true);
                    $(".js-data-sku-ajax").append(newState).trigger('change');
                }
            }

            $('#upcCode').val(data.upc ? data.upc : '');
            $('#category').attr('disabled', false).val(data.category ? data.category : '').trigger("change");
            $("#subCategory option[value='" + data.subCategory + "']").attr("selected", true);
            $('#size').val(data.size ? data.size : '');
            $('#unit').val(data.units ? data.units : '');
            $('#alc_content').val(data.alcContent ? data.alcContent : '');
            $('#region').val(data.region ? data.region : '');
            $('#appellation').val(data.appellation ? data.appellation : '');
            $('.country_list').val(data.country ? data.country : '');
            $('#year').val(data.year ? data.year : '');
            $('#varietal').val(data.varietal ? data.varietal : '');
            $('#itemDescription').attr('disabled', false).val(data.description ? data.description : '');
            $('#color').val(data.color ? data.color : '');
            $('#body').val(data.body ? data.body : '');
            $('#originalPrice').val(data.price.toString() ? data.price : '');
            $('#discountPrice').val(data.price.toString() ? data.price : '');
            $('#aroma').val(data.aromaAndFlavor ? data.aromaAndFlavor : '');
            $('#foodPairings').val(data.pairings ? data.pairings : '');
            $("#isSponsored").attr("checked", data.isSponsored);
            $("#isVolumeDiscountEligible").attr("checked", data.isVolumeDiscountEligible);
            $("#isTaxable").attr("checked", data.isTaxable);
            $('#productImage').attr('src', data.productPhoto ? base_url + 'storages/bevvi/download/' + data.productPhoto.file.filename : 'images/no-img.png');
            $('#loader_img').show();
            if (!data.productPhoto) {
                $('#loader_img').hide();
            }
            $('#productImage').on('load', function () {
                $('#loader_img').hide();
            });
        }
    }).on('select2:unselect', function (e) {
        disableAllFields();
    });

    //Accept number only in input
    jQuery('.number').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    //set Datepicker format
    $("#datepicker").datepicker({
        dateFormat: 'MM dd, yy',
        minDate: 0,
        changeMonth: true,
        changeYear: true
    });
    $("#datepicker1").datepicker({
        dateFormat: 'MM dd, yy',
        minDate: 0,
        changeMonth: true,
        changeYear: true
    });
    $(".offer2").css('display', 'none'); //On page load hide error

    ///Load Country Drop down
    $.ajax({
        url: './js/country.json',
        dataType: "application/json",
        cache: false,
        complete: function (abc) {
            var list = '';
            $.each(JSON.parse(abc.responseText), function (name, code) {
                list = '<option value="' + code.name + '">' + code.name + '</option>';
                $(".country_list").append(list);
            });
        }
    });

    ///Category according change field list on next page
    $("#category").change(function () {
        var category = $('#category').find(":selected").val();
        $('.addOffer').removeClass('active');
        $('.categoryBread').addClass('active').removeClass('hidden').html($("#category").val());
        switch (category) {
            case 'Wine':
                $(".subCategory").show()
                $("#subCategory").show().find("option").hide().attr('selected', false);
                $("#subCategory option[data-category='Wine']").show().eq(0).attr('selected', true);
                $("#div_alc_content").show();
                $("#div_region").show();
                $("#div_country").show();
                $("#div_year").show();
                $("#div_varietal").show();
                $("#div_appellation").show();
                $("#div_description").hide();
                $("#div_product_notes").show();
                break;

            case 'Liquor':
                $(".subCategory").show()
                $("#subCategory").show().find("option").hide().attr('selected', false);
                $("#subCategory option[data-category='Liquor']").show().eq(0).attr("selected", true)
                $("#div_alc_content").show();
                $("#div_region").hide();
                $("#div_country").show();
                $("#div_year").hide();
                $("#div_varietal").hide();
                $("#div_appellation").hide();
                $("#div_description").hide();
                $("#div_product_notes").hide();
                break;

            case 'Beer':
                $(".subCategory").hide()
                $("#subCategory").hide().find("option").hide();
                $("#div_alc_content").show();
                $("#div_region").hide();
                $("#div_country").show();
                $("#div_year").hide();
                $("#div_varietal").hide();
                $("#div_appellation").hide();
                $("#div_description").hide();
                $("#div_product_notes").hide();
                break;

            case 'Mixer':
                $(".subCategory").hide()
                $("#subCategory").hide().find("option").hide();
                $("#div_alc_content").hide();
                $("#div_region").hide();
                $("#div_country").hide();
                $("#div_year").hide();
                $("#div_varietal").hide();
                $("#div_appellation").hide();
                $("#div_product_notes").hide();
                $("#div_description").hide();
                break;
        }
    });

    $("#NextBtn").click(function () {
        var validation = FormValidate('next');
        if (validation === true) {
            $(".save-edit p.next").html('');
            $(".offer1").css('display', 'none');
            $(".card-head-edit").find("span").html($('#category').find(":selected").val());
            $(".offer2").css('display', 'block');
            $('.addOffer').removeClass('active');
            $('.categoryBread').addClass('active').removeClass('hidden').html($("#category").val());
        } else {
            $(".save-edit p.next").html('* Please fill in the above information to move forward. *');
            $("#NextBtn").attr('disable', true);
        }
    });

    $(".backBtn").click(function () {
        $(".offer1").css('display', 'block');
        $(".offer2").css('display', 'none');
    });

    $(".cancelBtn").click(function () {
        var validation = FormValidate('next');

        if (checkInputInForm) {
            showWarning = true;
            checkInputInForm = false;
        } else {
            showWarning = false;
        }
        window.location = "offers";
    });

    $('#cropperModel').on('hidden.bs.modal', function () {
        document.getElementById('fileInput').value = '';
        canvas.cropper('destroy');
    })
    $("#cropperModel").on('shown.bs.modal', function () {
        let img = new Image();
        img.onload = function () {
            context.canvas.height = img.height;
            context.canvas.width = img.width;
            context.drawImage(img, 0, 0);
            canvas.cropper('destroy');
            var cropper = canvas.cropper({
                aspectRatio: 9 / 16
            });
        };
        img.src = cropperImage;
    });

    ///Insert data into Database
    $(".createBtn").click(function (data) {
        var validation = FormValidate('create');

        var productName = $("#productName").val();
        var sku = $("#skuNumber").val();
        var upc = $("#upcCode").val();
        var category = $("#category").find(":selected").val();
        var oPrice = $("#originalPrice").val();
        var dPrice = $("#discountPrice").val();
        var quantity = $("#quantity").val();
        // var start_date = $("#datepicker").val();
        // var end_date = $("#datepicker1").val();
        var itemDescription = $("#itemDescription").val();
        var isSponsored = $("#isSponsored").is(":checked");
        var isVolumeDiscountEligible = $("#isVolumeDiscountEligible").is(":checked");

        var size = $("#size").val();
        var unit = $("#unit").val(); //Added by Us
        var alc_content = $("#alc_content").val();
        var region = $("#region").val();
        var appellation = $("#appellation").val();
        var country = $(".country_list").val();
        var year = $('#year').val();
        var subCategory = $('#subCategory').find(":selected").val();
        var varietal = $("#varietal").val();
        var description = $("#description").val();
        var color = $("#color").val();
        var body = $("#body").val();
        var aroma = $("#aroma").val();
        var foodPairings = $("#foodPairings").val();
        var productImage = $('#productImage').val();

        ///if all data is valid then insert data
        if (validation) {
            ///Insert record into product
            if (category == 'Wine') {
                var productData = {
                    "name": productName,
                    "upc": upc,
                    "sku": sku,
                    "category": category,
                    "size": size,
                    "units": unit,
                    "description": itemDescription,
                    "alcContent": alc_content,
                    "region": region,
                    "appellation": appellation,
                    "country": country,
                    "year": year,
                    "varietal": varietal,
                    "color": color,
                    "body": body,
                    "aromaAndFlavor": aroma,
                    "pairings": foodPairings,
                    "subCategory": subCategory
                };
            } else if (category == 'Liquor') {
                var productData = {
                    "name": productName,
                    "category": category,
                    "size": size,
                    "units": unit,
                    "description": itemDescription,
                    "upc": upc,
                    "sku": sku,
                    "alcContent": alc_content,
                    // "region": region,
                    "country": country,
                    "subCategory": subCategory
                    // "varietal": varietal
                };
            } else if (category == 'Beer') {
                var productData = {
                    "name": productName,
                    "category": category,
                    "size": size,
                    "units": unit,
                    "description": itemDescription,
                    "upc": upc,
                    "sku": sku,
                    "alcContent": alc_content,
                    "country": country
                    // "varietal": varietal
                };
            } else if (category == 'Mixer') {
                var productData = {
                    "name": productName,
                    "category": category,
                    "size": size,
                    "units": unit,
                    "description": itemDescription,
                    "upc": upc,
                    "sku": sku
                };
            }

            swal({
                title: "",
                text: "Create a New Offer ?",
                //type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Create",
                closeOnConfirm: true
            },
                function () {
                    productData.establishmentId = localStorage.getItem('establishmentId');
                    productData.isVolumeDiscountEligible = isVolumeDiscountEligible;
                    $.ajax({ ///Add or update product details
                        type: 'post',
                        url: base_url + 'products/upsertWithWhere?where={ "establishmentId" : "' + productData.establishmentId + '", "sku" : "' + sku + '"}',
                        data: JSON.stringify(productData),
                        dataType: 'json',
                        contentType: 'application/json',
                        success: function (data) {
                            $.ajax({ ///Add offer details
                                type: 'post',
                                url: base_url + 'offers',
                                data: JSON.stringify({
                                    "establishmentId": localStorage.getItem('establishmentId'),
                                    "description": description, ///wine type dont have description
                                    // "startsAt": moment(start_date + " 00:00:00", "MMM DD, YYYY HH:mm:ss").utc().format('YYYY-MM-DD HH:mm:ss'),
                                    // "endsAt": moment(end_date + " 23:59:59", "MMM DD, YYYY HH:mm:ss").utc().format('YYYY-MM-DD HH:mm:ss'),
                                    "originalPrice": oPrice,
                                    "salePrice": dPrice,
                                    "totalQty": quantity,
                                    "remQty": quantity,
                                    "active": true,
                                    "productId": data.id,
                                    isSponsored: isSponsored
                                }),
                                dataType: 'json',
                                contentType: 'application/json',
                                success: function (response) {
                                    if (!productImage) {
                                        swal({
                                            title: 'Success',
                                            text: 'New Offer Created Successfully',
                                            type: 'success',
                                            showCancelButton: false,
                                            confirmButtonClass: 'btn-danger',
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        }, function () {
                                            showWarning = false;
                                            window.location = 'offers';
                                        });
                                    }
                                    croppedImageDataURL = croppedImageDataURLToUpload.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
                                    var blob = b64toBlob(croppedImageDataURL, 'image/png');
                                    if (croppedImageDataURL != 'undefined') {
                                        var formData = new FormData();
                                        formData.append('image', blob, "image123" + Math.random() + '.png');
                                        formData.append("productId", data.id);
                                        console.log('Step 1');
                                        //Image upload to server
                                        $.ajax({
                                            url: img_url,
                                            type: 'POST',
                                            data: formData,
                                            async: false,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            success: function (status) {
                                                showWarning = false;
                                                window.location = 'offers';
                                                ///Add offer details
                                                // $.ajax({
                                                //     type: 'post',
                                                //     url: base_url + 'offers/publishOffer?access_token=' + access_token + '&establishmentId=' + localStorage.getItem("establishmentId") + '&offerId=' + response.id,
                                                //     success: function () {
                                                //         window.location = 'offers';
                                                //     },
                                                //     error: function (error) {
                                                //         console.log(error);
                                                //         console.log(error.status);

                                                //         //swal("", "There was error to send offer notification", "error");
                                                //         swal({
                                                //             title: error.status,
                                                //             text: error.message,
                                                //             type: "error",
                                                //             showCancelButton: false,
                                                //             confirmButtonClass: "btn-danger",
                                                //             confirmButtonText: "Ok",
                                                //             closeOnConfirm: true
                                                //         },
                                                //             function () { });
                                                //     }
                                                // });
                                            },
                                            error: function () {
                                                swal({
                                                    title: error.status,
                                                    text: error.message,
                                                    type: "error",
                                                    showCancelButton: false,
                                                    confirmButtonClass: "btn-danger",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true
                                                },
                                                    function () { });
                                            }
                                        });
                                    } else {
                                        swal({
                                            title: 'Success',
                                            text: 'New Offer Created Successfully',
                                            type: 'success',
                                            showCancelButton: false,
                                            confirmButtonClass: 'btn-danger',
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        }, function () {
                                            showWarning = false;
                                            window.location = 'offers';
                                        });
                                        // showWarning = false;
                                        // window.location = 'offers';
                                        // $.ajax({
                                        //     type: 'post',
                                        //     url: base_url + 'offers/publishOffer?access_token=' + access_token + '&establishmentId=' + localStorage.getItem("establishmentId") + '&offerId=' + response.id,
                                        //     success: function () {
                                        //         window.location = 'offers';
                                        //     },
                                        //     error: function (error) {
                                        //         console.log(error);
                                        //         console.log(error.status);

                                        //         //swal("", "There was error to send offer notification", "error");
                                        //         swal({
                                        //             title: error.status,
                                        //             text: error.message,
                                        //             type: "error",
                                        //             showCancelButton: false,
                                        //             confirmButtonClass: "btn-danger",
                                        //             confirmButtonText: "Ok",
                                        //             closeOnConfirm: true
                                        //         },
                                        //             function () { });
                                        //     }
                                        // });
                                    }
                                },
                                error: function (error) {
                                    console.log(error);
                                    //swal("", "There is some technical issue !", "error");
                                    if (error.responseJSON.error.status == 401) {
                                        swal({
                                            title: "",
                                            text: error.responseJSON.error.message,
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonClass: "btn-danger",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () { });
                                    }
                                }
                            });
                        },
                        error: function (response) {
                            console.log(response)
                            swal({
                                title: "",
                                text: response.responseJSON.error.message,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: false
                            })
                        }
                    });
                });
        } else {
            $(".save-edit p.save").html('* Please fill in the above information to move forward. *');
        }
    });

    var canvas = $("#canvas"),
        context = canvas.get(0).getContext("2d"),
        $result = $('#result');

    $('#fileInput').on('change', function () {

        var reader = new FileReader();
        reader.onload = function (e) {
            var fileInput = document.getElementById('fileInput');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
            if (!allowedExtensions.exec(filePath)) {
                alert('Selected file is not supported. Please select diffrent image');
                fileInput.value = '';
                return false;
            }
            else {
                $('#cropperModel').modal('show');
                cropperImage = e.target.result;
            }
        };
        reader.readAsDataURL(this.files[0]);
    });

    $('#btnSave').click(function () {
        croppedImageDataURL = croppedImageDataURLToUpload = canvas.cropper('getCroppedCanvas').toDataURL("image/png");
        document.getElementById("productImage").src = croppedImageDataURL;
        setTimeout(function () {
            canvas.cropper('reset');
            croppedImageDataURL = '';
        }, 200);
    });
});

var cropperImage;

function FormValidate(validate) {
    var valid = true;
    var productName = $("#productName").val();
    var sku = $("#skuNumber").val();
    var upc = $("#upcCode").val();
    var category = $("#category").find(":selected").val();
    var oPrice = $("#originalPrice").val();
    var dPrice = $("#discountPrice").val();
    var quantity = $("#quantity").val();
    // var start_date = $("#datepicker").val();
    // var end_date = $("#datepicker1").val();
    var itemDescription = $("#itemDescription").val();

    var size = $("#size").val();
    var unit = $("#unit").val();
    var alc_content = $("#alc_content").val();
    var region = $("#region").val();
    var appellation = $("#appellation").val();
    var country = $(".country_list").val();
    var year = $('#year').val();
    var itemCategory = $('#itemCategory').find(":selected").val();
    var varietal = $("#varietal").val();
    var description = $("#description").val();
    var color = $("#color").val();
    var body = $("#body").val();
    var aroma = $("#aroma").val();
    var foodPairings = $("#foodPairings").val();

    if (validate == 'next') {
        if (productName === null) {
            valid = false;
        } else {
            checkInputInForm = true;
        }
        if (sku === null) {
            valid = false;
        } else {
            checkInputInForm = true;
        }
        if (upc === null) {
            valid = false;
        }
        if (category === '') {
            valid = false;
        }
        if (oPrice === '') {
            valid = false;
        }
        if (dPrice === '') {
            valid = false;
        }
        if (quantity === '') {
            valid = false;
        }
        // if (start_date === '') {
        //     valid = false;
        // }
        // if (end_date === '') {
        //     valid = false;
        // }
        // if (start_date != '' && end_date != '') {
        //     if (moment(start_date) > moment(end_date)) {
        //         swal("", dateErrorMessage, "error");
        //         valid = false;
        //     }
        // }
        if (itemDescription === '') {
            valid = false;
        }

        //Validate form
        if (valid === false) {
            return false;
        } else {
            return true;
        }
    } else {
        if (category == 'Wine') {

            // if (region === '') {
            //     valid = false;
            // }
            // if (appellation === '') {
            //     valid = false;
            // }
            // if (country === '') {
            //     valid = false;
            // }
            // if (year === '') {
            //     valid = false;
            // }
            if (color == '') {
                valid = false;
            }
            // if (body == '') {
            //     valid = false;
            // }
            // if (aroma == '') {
            //     valid = false;
            // }
            // if (foodPairings == '') {
            //     valid = false;
            // }
        } else if (category == 'Liquor') {
            // if (region === '') {
            //     valid = false;
            // }
            // if (country === '') {
            //     valid = false;
            // }
            // if (description === '') {
            //     valid = false;
            // }

        } else if (category === 'Beer') {
            // if (description === '') {
            //     valid = false;
            // }
            // if (country === '') {
            //     valid = false;
            // }

        }
        //Below fields are in all category
        if (size === '') {
            // console.log("country, "+ country);
            // console.log("valid, "+ valid);


            valid = false;
        }
        if (unit === '') {
            valid = false;
        }
        // if (country === null) {
        //     valid = false;
        // }
        // if (alc_content == '') {
        //     valid = false;
        // }
        // if (varietal == '') {
        //     valid = false;
        // }

        //Validate form
        if (valid === false) {
            return false;
        } else {
            return true;
        }
    }
}

function disableAllFields() {
    $('#upcCode').attr('disabled', true).val('');
    $('#category').attr('disabled', true).val('');
    $('#subCategory').attr('disabled', true).val('');
    $('#size').attr('disabled', true).val('');
    $('#unit').attr('disabled', true).val('');
    $('#alc_content').attr('disabled', true).val('');
    $('#region').attr('disabled', true).val('');
    $('#appellation').attr('disabled', true).val('');
    $('.country_list').attr('disabled', true).val('');
    $('#year').attr('disabled', true).val('');
    $('#varietal').attr('disabled', true).val('');
    $('#itemDescription').attr('disabled', true).val('');
    $('#color').attr('disabled', true).val('');
    $('#body').attr('disabled', true).val('');
    $('#aroma').attr('disabled', true).val('');
    $('#foodPairings').attr('disabled', true).val('');
    $('#originalPrice').attr('disabled', true).val('');
    $('#discountPrice').attr('disabled', true).val('');
    $('#quantity').attr('disabled', true).val('');
    $('#datepicker').attr('disabled', true).val('');
    $('#datepicker1').attr('disabled', true).val('');
}

function enableAllFields() {
    $('#upcCode').attr('disabled', false);
    $('#category').attr('disabled', false);
    $('#subCategory').attr('disabled', false);
    $('#size').attr('disabled', false);
    $('#unit').attr('disabled', false);
    $('#alc_content').attr('disabled', false);
    $('#region').attr('disabled', false);
    $('#appellation').attr('disabled', false);
    $('.country_list').attr('disabled', false);
    $('#year').attr('disabled', false);
    $('#varietal').attr('disabled', false);
    $('#itemDescription').attr('disabled', false);
    $('#color').attr('disabled', false);
    $('#body').attr('disabled', false);
    $('#aroma').attr('disabled', false);
    $('#foodPairings').attr('disabled', false);
    $('#originalPrice').attr('disabled', false).val('');
    $('#discountPrice').attr('disabled', false);
    $('#quantity').attr('disabled', false);
    $('#datepicker').attr('disabled', false);
    $('#datepicker1').attr('disabled', false);
}