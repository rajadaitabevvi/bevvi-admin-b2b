/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global params */
var userid = localStorage.getItem('userId');
if (!userid) {
    window.location = 'login';
}
var offers = JSON.parse(localStorage.getItem("offers"));
$(document).ready(function () {

    //November 12, 2017
    sDate = localStorage.getItem("start_date");
    eDate = localStorage.getItem("end_date");

    $(".startDate").text(moment(sDate).format("MMMM DD, YYYY"));
    $(".endDate").text(moment(eDate).format("MMMM DD, YYYY"));

    offer = $.grep(offers, function (i) {
        return i.offer.id == params;
    });
    result = offer[0];
    var image = '';
    if (result.offer.product.productPhoto) {
        path = result.offer.product.productPhoto.file.path;
        container = path.split('/')[1];
        filename = path.split('/')[2];
        image = base_url + 'storages/' + container + '/download/' + filename;
        $("#productImage").attr('alt', result.offer.name);
    } else {
        image = 'images/no-img.png';
        $("#productImage").attr('alt', 'No Image');
    }

    $("#productImage").attr('src', image);
    if (result.bevviRevenue > 0) {
        //$(".edit a").attr("href","edit-offer?id="+params);
    } else {
        $(".edit a").attr("href", "edit-offer?id=" + params);
    }

    //Column 1 
    $("#productName").text(result.offer.product.name);
    $("#upcCode").text(result.offer.product.upc);
    $("#itemCategory").text(result.offer.product.category);
    $("#originalPrice").text("$" + result.offer.originalPrice.toFixed(2));
    var discountPrice = parseFloat(result.offer.salePrice);

    $("#discountPrice").text('$' + discountPrice.toFixed(2));
    $("#quantity").text(result.offer.remQty);
    $("#startDate").text(moment(result.offer.startsAt).format("MMMM DD, YYYY"));
    $("#endDate").text(moment(result.offer.endsAt).format("MMMM DD, YYYY"));

    //Column 2
    $("#bottleSold").text(parseInt(result.offer.totalQty) - parseInt(result.offer.remQty) + '/' + result.offer.totalQty);

    $("#revenue").text('$' + result.revenue.toFixed(2));
    if (result.revenue.toFixed(2) > 0) {
        $(".edit").addClass("hidden");
    } else {
        $(".edit").removeClass("hidden");
    }
    $("#totalDiscount").text('$' + result.discount.toFixed(2));
    $("#bevviRevenue").text('$' + result.bevviRevenue.toFixed(2));
    $("#salesTax").text('$' + result.tax.toFixed(2));

});