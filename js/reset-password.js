var id = "";
var token = "";

$(document).ready(function () {
    id = getQueryVariable('id');
    token = getQueryVariable('token');

    console.log("id", id);
    console.log("tokoen", token);

    if (!id || !token) {
        swal({
            title: "",
            text: 'Check the URL you have used. Please click on the link provided in the email',
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        });
        return false;
    }

    $("#saveBtn").click(function () {
        var password1 = $("#password1").val();
        var password2 = $("#password2").val();
        if (password1 == '' || password2 == '') {
            swal({
                title: "",
                text: 'Please fill both the fields to successfully set your new password',
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });
        } else if (password1 != password2) {
            swal({
                title: "",
                text: 'New Password does not match with Confirm Password',
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });
        } else {
            $.ajax({
                type: 'patch',
                url: base_url + 'bizaccounts/' + id + '?access_token=' + token,
                data: {
                    "password": encodeURIComponent(password1)
                },
                success: function (data) {
                    console.log(data);
                    //On success show pop up 
                    swal({
                        title: "",
                        text: 'Password changed successufully.',
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            window.location = 'login';
                        });
                }
            });
        }
    });
});
